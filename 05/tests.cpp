/**
 * @file tests.cpp
 *
 * @brief File used to test triangle function using path testing 
 *
 * @author Brian Burciaga T00566000
 * Contact: burciagab16@mytru.ca
 * 
 */

#include <iostream>
#include <string>
#include <gtest/gtest.h>
#include "triangle.hpp"

TEST(triangleTests, Gnode){
  //  Gnode = [ { 38, 39, 48 },
  ASSERT_EQ("NotATriangle", triangle(0, 55, 65));
  //            { 38, 39, 40, 41 },
  ASSERT_EQ("Equilateral", triangle(10, 10, 10));
  //            { 38, 39, 40, 43, 44 },
  ASSERT_EQ("Isosceles", triangle(4, 4, 7));
  //            { 38, 39, 40, 43, 46 }]
  ASSERT_EQ("Scalene", triangle(4, 5, 7));
}

TEST(triangleTests, Gedge){
  //  Gedge = [ { 38, 39, 48 },
  ASSERT_EQ("NotATriangle", triangle(14, 120, 199));
  //            { 38, 39, 40, 41 },
  ASSERT_EQ("Equilateral", triangle(160, 160, 160));
  //            { 38, 39, 40, 43, 44 },
  ASSERT_EQ("Isosceles", triangle(18, 12, 12));
  //            { 38, 39, 40, 43, 46 }]
  ASSERT_EQ("Scalene", triangle(18, 7, 12));
}

TEST(triangleTests, Gpaths){
  //  Gpaths = [ { 38, 39, 48 },
  ASSERT_EQ("NotATriangle", triangle(155, 165, 201));
  //            { 38, 39, 40, 41 },
  ASSERT_EQ("Equilateral", triangle(200, 200, 200));
  //            { 38, 39, 40, 43, 44 },
  ASSERT_EQ("Isosceles", triangle(120, 120, 150));
  //            { 38, 39, 40, 43, 46 }]
  ASSERT_EQ("Scalene", triangle(55, 65, 75));
}

TEST(triangleTests, Gchain){
  //  Gchain = [ { 38, 39, 48 },
  ASSERT_EQ("NotATriangle", triangle(399, 199, 199));
  //            { 38, 39, 40, 41 },
  ASSERT_EQ("Equilateral", triangle(19, 19, 19));
  //            { 38, 39, 40, 43, 44 },
  ASSERT_EQ("Isosceles", triangle(6, 7, 7));
  //            { 38, 39, 40, 43, 46 }]
  ASSERT_EQ("Scalene", triangle(180, 70, 120));
}

TEST(triangleTests, C0){
  //  C0 = [ { 38, 39, 48 },
  ASSERT_EQ("NotATriangle", triangle(1, 5, 9));
  //            { 38, 39, 40, 41 },
  ASSERT_EQ("Equilateral", triangle(1, 1, 1));
  //            { 38, 39, 40, 43, 44 },
  ASSERT_EQ("Isosceles", triangle(1, 2, 2));
  //            { 38, 39, 40, 43, 46 }]
  ASSERT_EQ("Scalene", triangle(6, 7, 9));
}

TEST(triangleTests, C1){
  //  C1 = [ { 38, 39, 48 },
  ASSERT_EQ("NotATriangle", triangle(18, 19, 199));
  //            { 38, 39, 40, 41 },
  ASSERT_EQ("Equilateral", triangle(5, 5, 5));
  //            { 38, 39, 40, 43, 44 },
  ASSERT_EQ("Isosceles", triangle(180, 110, 110));
  //            { 38, 39, 40, 43, 46 }]
  ASSERT_EQ("Scalene", triangle(29, 18, 23));
}

TEST(triangleTests, C2){
  //  C2 = [ { 38, 39, 48 },
  ASSERT_EQ("NotATriangle", triangle(69, 420, 8008));
  //            { 38, 39, 40, 41 },
  ASSERT_EQ("Equilateral", triangle(42, 42, 42));
  //            { 38, 39, 40, 43, 44 },
  ASSERT_EQ("Isosceles", triangle(80, 96, 96));
  //            { 38, 39, 40, 43, 46 }]
  ASSERT_EQ("Scalene", triangle(80, 90, 100));
}

TEST(triangleTests, Cinfinity){
  //  Cinfinity = [ { 38, 39, 48 },
  ASSERT_EQ("NotATriangle", triangle(199, 200, 201));
  //            { 38, 39, 40, 41 },
  ASSERT_EQ("Equilateral", triangle(98, 98, 98));
  //            { 38, 39, 40, 43, 44 },
  ASSERT_EQ("Isosceles", triangle(23, 24, 24));
  //            { 38, 39, 40, 43, 46 }]
  ASSERT_EQ("Scalene", triangle(101, 102, 103));
}
