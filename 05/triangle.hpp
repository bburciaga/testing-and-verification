/**
 * @file triangle.hpp
 *
 * @brief header file to call the triangle function in other directories
 *
 * @author Brian Burciaga T00566000
 * Contact: burciagab16@mytru.ca
 * 
 */
#include <iostream>
#include <string>

#ifndef TRIANGLE_HPP
#define TRIANGLE_HPP

std::string triangle(int a, int b, int c);

#endif

