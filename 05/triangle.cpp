/**
 * @file triangle.cpp
 *
 * @brief Function to return if the three inputs make up a specific triangle
 *
 * @author Brian Burciaga T00566000
 * Contact: burciagab16@mytru.ca
 * 
 */
#include <iostream>
#include <string>
#include "triangle.hpp"

/**
 * Helper function to check if the inputed values are valid
 * in regards to the bounds and triangle properties
 * @params int a length of side A
 * @params int b length of side B
 * @params int c length of side C
 * @returns true or false
 */
int isValidInput(int a, int b, int c) {
  if (a >= 1 && b >= 1 && c >= 1 && a <= 200 && b <= 200 && c <= 200) {
    if (a < b + c && b < a + c && c < a + b) {
      return 1;
    }
  }
  return 0;
}

/**
 * Function to return what type of Triangle is the output.
 * @params int a length of side A
 * @params int b length of side B
 * @params int c length of side C
 * @returns string value of either Equilateral, Iscosceles, Scalene or NotATriangle
 */
std::string triangle(int a, int b, int c) {
  if (isValidInput(a, b, c)) {
    if (a == b && b == c) {
      return std::string("Equilateral");
    }
    if (a == b || b == c || a == c) {
      return std::string("Isosceles");
    }
    return std::string("Scalene");
  }
  return std::string("NotATriangle");
}

