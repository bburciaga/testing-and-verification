# Lab 5: Triangle Function

## Triangle Function

The function has three inputs and one output.
  - Equilateral
  - Isosceles
  - Scalene
  - NotATriangle

## Tests

The test file consists of the following tests:
  - Gnode
  - Gedge
  - Gpaths
  - Gchain
  - C0
  - C1
  - C2
  - Cinfinity

### Path Graph

```mermaid
flowchart TD;
  A[Input a, b, and c]-->B{isValidInput?};
  B-->|No| H[return NotATriangle];
  B-->|No| C{if a==b and b==c};
  C-->|Yes| D[return Equilateral];
  C-->|Yes| E{if a==b and b==c};
  E-->|Yes| F[return Isosceles];
  E-->|No| G[return Scalene];
```

### DD Graph

```mermaid
flowchart TD;
  38-->39;
  39-->48;
  39-->40;
  40-->41;
  40-->43;
  43-->44;
  43-->46;
```

