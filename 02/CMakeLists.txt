# minimum version to use for cmake
cmake_minimum_required(VERSION 3.10)
# project name
project(lab2)

# include package for project
find_package(GTest REQUIRED)
include_directories(${GTEST_INCLUDE_DIRS})

add_library(triangle_library triangle.cpp triangle.hpp)

# generating executable for the cpp program
add_executable(runTests main.cpp tests.cpp triangle.cpp)
target_link_libraries(runTests triangle_library)
target_link_libraries(runTests ${GTEST_LIBRARIES} pthread)
