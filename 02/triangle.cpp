#include <iostream>
#include <string>
#include "triangle.hpp"

/**
 * Function to return what type of Triangle is the output.
 * @params int a length of side A
 * @params int b length of side B
 * @params int c length of side C
 * @returns string value of either Equilateral, Iscosceles, Scalene or NotATriangle
 */
std::string triangle(int a, int b, int c) {
  if (a >= 1 && b >= 1 && c >= 1 && a <= 200 && b <= 200 && c <= 200) {
    if (a < b + c && b < a + c && c < a + b) {
      if (a == b && b == c) {
        return std::string("Equilateral");
      }
      else if (a == b || b == c || a == c) {
        return std::string("Isosceles");
      }
      return std::string("Scalene");
    }
  }
  return std::string("NotATriangle");
}

