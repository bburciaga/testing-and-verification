# Lab 2: Triangle Tests

## Triangle Function

The function has three inputs and one output.
  - Equilateral
  - Isosceles
  - Scalene
  - NotATriangle

## Tests

The test file consists of the following tests:
  - Boundary Value Testing
  - Robust Value Boundary Testing
  - Weak-Case Value Boundary Testing
  - Robust Weak-Case Testing
  - Special Value Testing
  - Randomized Testing

### Boundary Value Testing

> The number of tests is 4n + 1.
> n being number of inputs.
> Testing values from minimum to maximum.

The triangle function has 3 inputs which means
we have 13 tests.

### Robust Boundary Value Testing

> The number of tests is 6n + 1.
> n being number of inputs
> Testing values from minimum - 1 to maximum + 1.

The triangle function has 3 inputs which means
we have 19 tests.

### Worst-Case Boundary Value Testing

> The number of tests is 5^n.
> n being number of inputs.
> Testing values from minimum to maximum.
> Test all valid combinations of values.

The triangle function has 3 inputs which means 
we have 125 tests.

### Robust Worst-Case Boundary Value Testing

> The number of tests is 7^n.
> n being number of inputs.
> Testing values from minimum - 1 to maximum + 1.
> Test all combinations of testing values.

The triangle function has 3 inputs which means 
we have 343 tests.

### Special Value Testing

> Similar to Normal BVT, but we also use specific
> values for our tests as well.
> At least 4n + 1 tests.
> n being number of inputs.

The triangle function has 3 inputs which means
we have at least 13 tests. I decided to add
4 more tests.

### Random Value Testing

I specifically used a for loop to count to 
20. I use the triangle function to get strings
based on random inputs of a, b, and c. I then
use Google Tests to check if that's correct.

