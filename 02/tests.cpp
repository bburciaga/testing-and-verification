#include <iostream>
#include <string>
#include <stdlib.h>
#include <gtest/gtest.h>
#include "triangle.hpp"

TEST(TriangleTests, BVT) {
  //  Case 1  min    nom   nom 
  ASSERT_EQ("Equilateral", triangle(1, 1, 1));
  //  Case 2  min+   nom   nom
  ASSERT_EQ("NotATriangle", triangle(2, 1, 1));
  //  Case 3  max    nom   nom
  ASSERT_EQ("NotATriangle", triangle(200, 1, 1));
  //  Case 4  max-   nom   nom
  ASSERT_EQ("NotATriangle", triangle(199, 1, 1));
  //  ---------------------- 
  //  Case 5  nom    min   nom
  ASSERT_EQ("NotATriangle", triangle(5, 1, 3));
  //  Case 6  nom    min+  nom
  ASSERT_EQ("NotATriangle", triangle(5, 2, 3));
  //  Case 7  nom    max   nom
  ASSERT_EQ("NotATriangle", triangle(11, 200, 91));
  //  Case 8  nom    max-  nom
  ASSERT_EQ("NotATriangle", triangle(11, 199, 91));
  //  ---------------------- 
  //  Case 9  nom    nom   min 
  ASSERT_EQ("NotATriangle", triangle(4, 8, 1));
  //  Case 10 nom    nom   min+ 
  ASSERT_EQ("NotATriangle", triangle(16, 30, 2));
  //  Case 11 nom    nom   max
  ASSERT_EQ("Scalene", triangle(45, 160, 200));
  //  Case 12 nom    nom   max-
  ASSERT_EQ("Equilateral", triangle(199, 199, 199));
  //  ---------------------- 
  //  Case 13 nom    nom   nom 
  ASSERT_EQ("NotATriangle", triangle(53, 167, 35));
}

TEST(TriangleTests, Robust_BVT) {
  // Case 1   min     nom   nom
  ASSERT_EQ("Isosceles", triangle(1,6,6));
  // Case 2   min+    nom   nom
  ASSERT_EQ("NotATriangle", triangle(2,2,20));
  // Case 3   min-    nom   nom
  ASSERT_EQ("NotATriangle", triangle(0,23,54));
  // Case 4   max     nom   nom
  ASSERT_EQ("NotATriangle", triangle(200,100,50));
  // Case 5   max+    nom   nom
  ASSERT_EQ("Isosceles", triangle(199,150,150));
  // Case 6   max-    nom   nom
  ASSERT_EQ("NotATriangle", triangle(201,10,11));
  //  -----------------------
  // Case 7    nom    min    nom
  ASSERT_EQ("Equilateral", triangle(1,1,1));
  // Case 8    nom    min+   nom
  ASSERT_EQ("NotATriangle", triangle(3,2,1));
  // Case 9    nom    min-   nom
  ASSERT_EQ("NotATriangle", triangle(55,0,23));
  // Case 10   nom    max    nom
  ASSERT_EQ("NotATriangle", triangle(100,200,100));
  // Case 11   nom    max+   nom
  ASSERT_EQ("NotATriangle", triangle(49,199,79));
  // Case 12   nom    max-   nom
  ASSERT_EQ("NotATriangle", triangle(23,201,50));
  //  -----------------------
  // Case 13   nom    nom   min
  ASSERT_EQ("NotATriangle", triangle(5,19,1));
  // Case 14   nom    nom   min+
  ASSERT_EQ("NotATriangle", triangle(23,49,2));
  // Case 15   nom    nom   min-
  ASSERT_EQ("NotATriangle", triangle(12,58,0));
  // Case 16   nom    nom   max
  ASSERT_EQ("NotATriangle", triangle(50,50,200));
  // Case 17   nom    nom   max+
  ASSERT_EQ("NotATriangle", triangle(23,43,199));
  // Case 18   nom    nom   min-
  ASSERT_EQ("NotATriangle", triangle(95,23,201));
  //  -----------------------
  // Case 19   min-    nom   nom
  ASSERT_EQ("NotATriangle", triangle(50,50,150));
}

TEST(TriangleTests, WorstCase_BVT) {
  // Case 1     min     min     min
  ASSERT_EQ("Equilateral", triangle(1, 1, 1));
  // Case 2     min     min     min+
  ASSERT_EQ("NotATriangle", triangle(1, 1, 2));
  // Case 3     min     min     nom
  ASSERT_EQ("NotATriangle", triangle(1, 1, 32));
  // Case 4     min     min     max-
  ASSERT_EQ("NotATriangle", triangle(1, 1, 199));
  // Case 5     min     min     max
  ASSERT_EQ("NotATriangle", triangle(1, 1, 200));
  //  ----------------------------

  // Case 6     min     min+     min
  ASSERT_EQ("NotATriangle", triangle(1, 2, 1));
  // Case 7     min     min+     min+
  ASSERT_EQ("Isosceles", triangle(1, 2, 2));
  // Case 8     min     min+     nom
  ASSERT_EQ("NotATriangle", triangle(1, 2, 65));
  // Case 9     min     min+     max-
  ASSERT_EQ("NotATriangle", triangle(1, 2, 199));
  // Case 10     min     min+     max
  ASSERT_EQ("NotATriangle", triangle(1, 2, 200));
  //  ----------------------------

  // Case 11     min     nom     min
  ASSERT_EQ("NotATriangle", triangle(1, 5, 1));
  // Case 12     min     nom     min+
  ASSERT_EQ("NotATriangle", triangle(1, 3, 2));
  // Case 13     min     nom     nom
  ASSERT_EQ("NotATriangle", triangle(1, 3, 9));
  // Case 14     min     nom     max-
  ASSERT_EQ("NotATriangle", triangle(1, 166, 199));
  // Case 15     min     nom     max
  ASSERT_EQ("NotATriangle", triangle(1, 167, 200));
  //  ----------------------------

  // Case 16     min     max-     min
  ASSERT_EQ("NotATriangle", triangle(1, 199, 1));
  // Case 17     min     max-     min+
  ASSERT_EQ("NotATriangle", triangle(1, 199, 2));
  // Case 18     min     max-     nom
  ASSERT_EQ("NotATriangle", triangle(1, 199, 77));
  // Case 19     min     max-     max-
  ASSERT_EQ("Isosceles", triangle(1, 199, 199));
  // Case 20     min     max-     max
  ASSERT_EQ("NotATriangle", triangle(1, 199, 200));
  //  ----------------------------

  // Case 21     min     max     min
  ASSERT_EQ("NotATriangle", triangle(1, 200, 1));
  // Case 22     min     max     min+
  ASSERT_EQ("NotATriangle", triangle(1, 200, 2));
  // Case 23     min     max     nom
  ASSERT_EQ("NotATriangle", triangle(1, 200, 78));
  // Case 24     min     max     max-
  ASSERT_EQ("NotATriangle", triangle(1, 200, 199));
  // Case 25     min     max     max
  ASSERT_EQ("Isosceles", triangle(1, 200, 200));
  //  ----------------------------

  // Case 26     min+     min     min
  ASSERT_EQ("NotATriangle", triangle(2, 1, 1));
  // Case 27     min+     min     min+
  ASSERT_EQ("Isosceles", triangle(2, 1, 2));
  // Case 28     min+     min     nom
  ASSERT_EQ("NotATriangle", triangle(2, 1, 79));
  // Case 29     min+     min     max-
  ASSERT_EQ("NotATriangle", triangle(2, 1, 199));
  // Case 30     min+     min     max
  ASSERT_EQ("NotATriangle", triangle(2, 1, 200));
  //  ----------------------------

  // Case 31     min+     min+     min
  ASSERT_EQ("Isosceles", triangle(2, 2, 1));
  // Case 32     min+     min+     min+
  ASSERT_EQ("Equilateral", triangle(2, 2, 2));
  // Case 33     min+     min+     nom
  ASSERT_EQ("NotATriangle", triangle(2, 2, 80));
  // Case 34     min+     min+     max-
  ASSERT_EQ("NotATriangle", triangle(2, 2, 199));
  // Case 35     min+     min+     max
  ASSERT_EQ("NotATriangle", triangle(2, 2, 200));
  //  ----------------------------

  // Case 36     min+     nom    min
  ASSERT_EQ("NotATriangle", triangle(2, 3, 1));
  // Case 37     min+     nom     min+
  ASSERT_EQ("NotATriangle", triangle(2, 4, 2));
  // Case 38     min+     nom     nom
  ASSERT_EQ("Scalene", triangle(2, 5, 6));
  // Case 39     min+     nom     max-
  ASSERT_EQ("NotATriangle", triangle(2, 7, 199));
  // Case 40     min+     nom     max
  ASSERT_EQ("NotATriangle", triangle(2, 8, 200));
  //  ----------------------------

  // Case 41     min+     max-     min
  ASSERT_EQ("NotATriangle", triangle(2, 199, 1));
  // Case 42     min+     max-     min+
  ASSERT_EQ("NotATriangle", triangle(2, 199, 2));
  // Case 43     min+     max-     nom
  ASSERT_EQ("NotATriangle", triangle(2, 199, 81));
  // Case 44     min+     max-     max-
  ASSERT_EQ("Isosceles", triangle(2, 199, 199));
  // Case 45     min+     max-     max
  ASSERT_EQ("Scalene", triangle(2, 199, 200));
  //  ----------------------------

  // Case 46     min+     max     min
  ASSERT_EQ("NotATriangle", triangle(2, 200, 1));
  // Case 47     min+     max     min+
  ASSERT_EQ("NotATriangle", triangle(2, 200, 2));
  // Case 48     min+     max     nom
  ASSERT_EQ("NotATriangle", triangle(2, 200, 82));
  // Case 49     min+     max     max-
  ASSERT_EQ("Scalene", triangle(2, 200, 199));
  // Case 50     min+     max     max
  ASSERT_EQ("Isosceles", triangle(2, 200, 200));
  //  ----------------------------

  // Case 51     nom     min     min
  ASSERT_EQ("Equilateral", triangle(1, 1, 1));
  // Case 52     nom     min     min+
  ASSERT_EQ("NotATriangle", triangle(133, 1, 2));
  // Case 53     nom     min     nom
  ASSERT_EQ("Isosceles", triangle(13, 1, 13));
  // Case 54     nom     min     max-
  ASSERT_EQ("NotATriangle", triangle(123, 1, 199));
  // Case 55     nom     min     max
  ASSERT_EQ("NotATriangle", triangle(35, 1, 200));
  //  ----------------------------

  // Case 56     nom     min+     min
  ASSERT_EQ("NotATriangle", triangle(3, 2, 1));
  // Case 57     nom     min+     min+
  ASSERT_EQ("Isosceles", triangle(3, 2, 2));
  // Case 58     nom     min+     nom
  ASSERT_EQ("Scalene", triangle(3, 2, 4));
  // Case 59     nom     min+     max-
  ASSERT_EQ("NotATriangle", triangle(164, 2, 199));
  // Case 60     nom     min+     max
  ASSERT_EQ("NotATriangle", triangle(150, 2, 200));
  //  ----------------------------

  // Case 61     nom     nom     min
  ASSERT_EQ("NotATriangle", triangle(54, 23, 1));
  // Case 62     nom     nom     min+
  ASSERT_EQ("Equilateral", triangle(2, 2, 2));
  // Case 63     nom     nom     nom
  ASSERT_EQ("Scalene", triangle(3, 4, 5));
  // Case 64     nom     nom     max-
  ASSERT_EQ("Scalene", triangle(88, 122, 199));
  // Case 65     nom     nom     max
  ASSERT_EQ("Isosceles", triangle(123, 123, 200));
  //  ----------------------------

  // Case 66     nom     max-     min
  ASSERT_EQ("NotATriangle", triangle(6, 199, 1));
  // Case 67     nom     max-     min+
  ASSERT_EQ("NotATriangle", triangle(74, 199, 2));
  // Case 68     nom     max-     nom
  ASSERT_EQ("Isosceles", triangle(150, 199, 150));
  // Case 69     nom     max-     max-
  ASSERT_EQ("Isosceles", triangle(50, 199, 199));
  // Case 70     nom     max-     max
  ASSERT_EQ("Scalene", triangle(23, 199, 200));
  //  ----------------------------

  // Case 71     nom     max     min
  ASSERT_EQ("NotATriangle", triangle(100, 200, 1));
  // Case 72     nom     max     min+
  ASSERT_EQ("NotATriangle", triangle(69, 200, 2));
  // Case 73     nom     max     nom
  ASSERT_EQ("NotATriangle", triangle(13, 200, 54));
  // Case 74     nom     max     max-
  ASSERT_EQ("Scalene", triangle(16, 200, 199));
  // Case 75     nom     max     max
  ASSERT_EQ("Isosceles", triangle(100, 200, 200));
  //  ----------------------------

  // Case 76     max-     min     min
  ASSERT_EQ("NotATriangle", triangle(199, 1, 1));
  // Case 77     max-     min     min+
  ASSERT_EQ("NotATriangle", triangle(199, 1, 2));
  // Case 78     max-     min     nom
  ASSERT_EQ("NotATriangle", triangle(199, 1, 83));
  // Case 79     max-     min     max-
  ASSERT_EQ("Isosceles", triangle(199, 1, 199));
  // Case 80     max-     min     max
  ASSERT_EQ("NotATriangle", triangle(199, 1, 200));
  //  ----------------------------

  // Case 81     max-     min+     min
  ASSERT_EQ("NotATriangle", triangle(199, 2, 1));
  // Case 82     max-     min+     min+
  ASSERT_EQ("NotATriangle", triangle(199, 2, 2));
  // Case 83     max-     min+     nom
  ASSERT_EQ("NotATriangle", triangle(199, 2, 84));
  // Case 84     max-     min+     max-
  ASSERT_EQ("Isosceles", triangle(199, 2, 199));
  // Case 85     max-     min+     max
  ASSERT_EQ("Scalene", triangle(199, 2, 200));
  //  ----------------------------

  // Case 86     max-     nom     min
  ASSERT_EQ("NotATriangle", triangle(199, 9, 1));
  // Case 87     max-     nom     min+
  ASSERT_EQ("NotATriangle", triangle(199, 10, 2));
  // Case 88     max-     nom     nom
  ASSERT_EQ("NotATriangle", triangle(199, 11, 12));
  // Case 89     max-     nom     max-
  ASSERT_EQ("Isosceles", triangle(199, 13, 199));
  // Case 90     max-     nom     max
  ASSERT_EQ("Scalene", triangle(199, 14, 200));
  //  ----------------------------

  // Case 91     max-     max-     min
  ASSERT_EQ("Isosceles", triangle(199, 199, 1));
  // Case 92     max-     max-     min+
  ASSERT_EQ("Isosceles", triangle(199, 199, 2));
  // Case 93     max-     max-     nom
  ASSERT_EQ("Isosceles", triangle(199, 199, 85));
  // Case 94     max-     max-     max-
  ASSERT_EQ("Equilateral", triangle(199, 199, 199));
  // Case 95     max-     max-     max
  ASSERT_EQ("Isosceles", triangle(199, 199, 200));
  //  ----------------------------

  // Case 96     max-     max     min
  ASSERT_EQ("NotATriangle", triangle(199, 200, 1));
  // Case 97     max-     max     min+
  ASSERT_EQ("Scalene", triangle(199, 200, 2));
  // Case 98     max-     max     nom
  ASSERT_EQ("Scalene", triangle(199, 200, 86));
  // Case 99     max-     max     max-
  ASSERT_EQ("Isosceles", triangle(199, 200, 199));
  // Case 100     max-     max     max
  ASSERT_EQ("Isosceles", triangle(199, 200, 200));
  //  ----------------------------

  // Case 101     max     min     min
  ASSERT_EQ("NotATriangle", triangle(200, 1, 1));
  // Case 102     max     min     min+
  ASSERT_EQ("NotATriangle", triangle(200, 1, 2));
  // Case 103     max     min     nom
  ASSERT_EQ("NotATriangle", triangle(200, 1, 87));
  // Case 104     max     min     max-
  ASSERT_EQ("NotATriangle", triangle(200, 1, 199));
  // Case 105     max     min     max
  ASSERT_EQ("Isosceles", triangle(200, 1, 200));
  //  ----------------------------

  // Case 106     max     min+     min
  ASSERT_EQ("NotATriangle", triangle(200, 2, 1));
  // Case 107     max     min+     min+
  ASSERT_EQ("NotATriangle", triangle(200, 2, 2));
  // Case 108     max     min+     nom
  ASSERT_EQ("NotATriangle", triangle(200, 2, 88));
  // Case 109     max     min+     max-
  ASSERT_EQ("Scalene", triangle(200, 2, 199));
  // Case 110     max     min+     max
  ASSERT_EQ("Isosceles", triangle(200, 2, 200));
  //  ----------------------------

  // Case 111     max     nom     min
  ASSERT_EQ("NotATriangle", triangle(200, 15, 1));
  // Case 112     max     nom     min+
  ASSERT_EQ("NotATriangle", triangle(200, 16, 2));
  // Case 113     max     nom     nom
  ASSERT_EQ("NotATriangle", triangle(200, 17, 18));
  // Case 114     max     nom     max-
  ASSERT_EQ("Scalene", triangle(200, 19, 199));
  // Case 115     max     nom     max
  ASSERT_EQ("Isosceles", triangle(200, 20, 200));
  //  ----------------------------

  // Case 116     max     max-     min
  ASSERT_EQ("NotATriangle", triangle(200, 199, 1));
  // Case 117     max     max-     min+
  ASSERT_EQ("Scalene", triangle(200, 199, 2));
  // Case 118     max     max-     nom
  ASSERT_EQ("Scalene", triangle(200, 199, 89));
  // Case 119     max     max-     max-
  ASSERT_EQ("Isosceles", triangle(200, 199, 199));
  // Case 120     max     max-     max
  ASSERT_EQ("Isosceles", triangle(200, 199, 200));
  //  ----------------------------

  // Case 121     max     max     min
  ASSERT_EQ("Isosceles", triangle(200, 200, 1));
  // Case 122     max     max     min+
  ASSERT_EQ("Isosceles", triangle(200, 200, 2));
  // Case 123     max     max     nom
  ASSERT_EQ("Isosceles", triangle(200, 200, 91));
  // Case 124     max     max     max-
  ASSERT_EQ("Isosceles", triangle(200, 200, 199));
  // Case 125     max     max     max
  ASSERT_EQ("Equilateral", triangle(200, 200, 200));
}

TEST(TriangleTests, Robust_Worst_Case_BVT) {
  // Case 1     min-     min-     min-
  ASSERT_EQ("NotATriangle", triangle(0, 0, 0));
  // Case 2     min-     min-     min
  ASSERT_EQ("NotATriangle", triangle(0, 0, 1));
  // Case 3     min-     min-     min+
  ASSERT_EQ("NotATriangle", triangle(0, 0, 2));
  // Case 4     min-     min-     nom
  ASSERT_EQ("NotATriangle", triangle(0, 0, 92));
  // Case 5     min-     min-     max-
  ASSERT_EQ("NotATriangle", triangle(0, 0, 199));
  // Case 6     min-     min-     max
  ASSERT_EQ("NotATriangle", triangle(0, 0, 200));
  // Case 7     min-     min-     max+
  ASSERT_EQ("NotATriangle", triangle(0, 0, 201));
  //  ----------------------------

  // Case 8     min-     min     min-
  ASSERT_EQ("NotATriangle", triangle(0, 1, 0));
  // Case 9     min-     min     min
  ASSERT_EQ("NotATriangle", triangle(0, 1, 1));
  // Case 10     min-     min     min+
  ASSERT_EQ("NotATriangle", triangle(0, 1, 2));
  // Case 11     min-     min     nom
  ASSERT_EQ("NotATriangle", triangle(0, 1, 93));
  // Case 12     min-     min     max-
  ASSERT_EQ("NotATriangle", triangle(0, 1, 199));
  // Case 13     min-     min     max
  ASSERT_EQ("NotATriangle", triangle(0, 1, 200));
  // Case 14     min-     min     max+
  ASSERT_EQ("NotATriangle", triangle(0, 1, 201));
  //  ----------------------------

  // Case 15     min-     min+     min-
  ASSERT_EQ("NotATriangle", triangle(0, 2, 0));
  // Case 16     min-     min+     min
  ASSERT_EQ("NotATriangle", triangle(0, 2, 1));
  // Case 17     min-     min+     min+
  ASSERT_EQ("NotATriangle", triangle(0, 2, 2));
  // Case 18     min-     min+     nom
  ASSERT_EQ("NotATriangle", triangle(0, 2, 94));
  // Case 19     min-     min+     max-
  ASSERT_EQ("NotATriangle", triangle(0, 2, 199));
  // Case 20     min-     min+     max
  ASSERT_EQ("NotATriangle", triangle(0, 2, 200));
  // Case 21     min-     min+     max+
  ASSERT_EQ("NotATriangle", triangle(0, 2, 201));
  //  ----------------------------

  // Case 22     min-     nom     min-
  ASSERT_EQ("NotATriangle", triangle(0, 21, 0));
  // Case 23     min-     nom     min
  ASSERT_EQ("NotATriangle", triangle(0, 22, 1));
  // Case 24     min-     nom     min+
  ASSERT_EQ("NotATriangle", triangle(0, 23, 2));
  // Case 25     min-     nom     nom
  ASSERT_EQ("NotATriangle", triangle(0, 24, 95));
  // Case 26     min-     nom     max-
  ASSERT_EQ("NotATriangle", triangle(0, 25, 199));
  // Case 27     min-     nom     max
  ASSERT_EQ("NotATriangle", triangle(0, 26, 200));
  // Case 28     min-     nom     max+
  ASSERT_EQ("NotATriangle", triangle(0, 27, 201));
  //  ----------------------------

  // Case 29     min-     max-     min-
  ASSERT_EQ("NotATriangle", triangle(0, 199, 0));
  // Case 30     min-     max-     min
  ASSERT_EQ("NotATriangle", triangle(0, 199, 1));
  // Case 31     min-     max-     min+
  ASSERT_EQ("NotATriangle", triangle(0, 199, 2));
  // Case 32     min-     max-     nom
  ASSERT_EQ("NotATriangle", triangle(0, 199, 96));
  // Case 33     min-     max-     max-
  ASSERT_EQ("NotATriangle", triangle(0, 199, 199));
  // Case 34     min-     max-     max
  ASSERT_EQ("NotATriangle", triangle(0, 199, 200));
  // Case 35     min-     max-     max+
  ASSERT_EQ("NotATriangle", triangle(0, 199, 201));
  //  ----------------------------

  // Case 36     min-     max     min-
  ASSERT_EQ("NotATriangle", triangle(0, 200, 0));
  // Case 37     min-     max     min
  ASSERT_EQ("NotATriangle", triangle(0, 200, 1));
  // Case 38     min-     max     min+
  ASSERT_EQ("NotATriangle", triangle(0, 200, 2));
  // Case 39     min-     max     nom
  ASSERT_EQ("NotATriangle", triangle(0, 200, 97));
  // Case 40     min-     max     max-
  ASSERT_EQ("NotATriangle", triangle(0, 200, 199));
  // Case 41     min-     max     max
  ASSERT_EQ("NotATriangle", triangle(0, 200, 200));
  // Case 42     min-     max     max+
  ASSERT_EQ("NotATriangle", triangle(0, 200, 201));
  //  ----------------------------

  // Case 43     min-     max+     min-
  ASSERT_EQ("NotATriangle", triangle(0, 201, 0));
  // Case 44     min-     max+     min
  ASSERT_EQ("NotATriangle", triangle(0, 201, 1));
  // Case 45     min-     max+     min+
  ASSERT_EQ("NotATriangle", triangle(0, 201, 2));
  // Case 46     min-     max+     nom
  ASSERT_EQ("NotATriangle", triangle(0, 201, 98));
  // Case 47     min-     max+     max-
  ASSERT_EQ("NotATriangle", triangle(0, 201, 199));
  // Case 48     min-     max+     max
  ASSERT_EQ("NotATriangle", triangle(0, 201, 200));
  // Case 49     min-     max+     max+
  ASSERT_EQ("NotATriangle", triangle(0, 201, 201));
  //  ----------------------------

  // Case 50     min     min-     min-
  ASSERT_EQ("NotATriangle", triangle(1, 0, 0));
  // Case 51     min     min-     min
  ASSERT_EQ("NotATriangle", triangle(1, 0, 1));
  // Case 52     min     min-     min+
  ASSERT_EQ("NotATriangle", triangle(1, 0, 2));
  // Case 53     min     min-     nom
  ASSERT_EQ("NotATriangle", triangle(1, 0, 99));
  // Case 54     min     min-     max-
  ASSERT_EQ("NotATriangle", triangle(1, 0, 199));
  // Case 55     min     min-     max
  ASSERT_EQ("NotATriangle", triangle(1, 0, 200));
  // Case 56     min     min-     max+
  ASSERT_EQ("NotATriangle", triangle(1, 0, 201));
  //  ----------------------------

  // Case 57     min     min     min-
  ASSERT_EQ("NotATriangle", triangle(1, 1, 0));
  // Case 58     min     min     min
  ASSERT_EQ("Equilateral", triangle(1, 1, 1));
  // Case 59     min     min     min+
  ASSERT_EQ("NotATriangle", triangle(1, 1, 2));
  // Case 60     min     min     nom
  ASSERT_EQ("NotATriangle", triangle(1, 1, 100));
  // Case 61     min     min     max-
  ASSERT_EQ("NotATriangle", triangle(1, 1, 199));
  // Case 62     min     min     max
  ASSERT_EQ("NotATriangle", triangle(1, 1, 200));
  // Case 63     min     min     max+
  ASSERT_EQ("NotATriangle", triangle(1, 1, 201));
  //  ----------------------------

  // Case 64     min     min+     min-
  ASSERT_EQ("NotATriangle", triangle(1, 2, 0));
  // Case 65     min     min+     min
  ASSERT_EQ("NotATriangle", triangle(1, 2, 1));
  // Case 66     min     min+     min+
  ASSERT_EQ("Isosceles", triangle(1, 2, 2));
  // Case 67     min     min+     nom
  ASSERT_EQ("NotATriangle", triangle(1, 2, 101));
  // Case 68     min     min+     max-
  ASSERT_EQ("NotATriangle", triangle(1, 2, 199));
  // Case 69     min     min+     max
  ASSERT_EQ("NotATriangle", triangle(1, 2, 200));
  // Case 70     min     min+     max+
  ASSERT_EQ("NotATriangle", triangle(1, 2, 201));
  //  ----------------------------

  // Case 71     min     nom     min-
  ASSERT_EQ("NotATriangle", triangle(1, 28, 0));
  // Case 72     min     nom     min
  ASSERT_EQ("NotATriangle", triangle(1, 29, 1));
  // Case 73     min     nom     min+
  ASSERT_EQ("NotATriangle", triangle(1, 30, 2));
  // Case 74     min     nom     nom
  ASSERT_EQ("NotATriangle", triangle(1, 31, 32));
  // Case 75     min     nom     max-
  ASSERT_EQ("NotATriangle", triangle(1, 33, 199));
  // Case 76     min     nom     max
  ASSERT_EQ("NotATriangle", triangle(1, 34, 200));
  // Case 77     min     nom     max+
  ASSERT_EQ("NotATriangle", triangle(1, 35, 201));
  //  ----------------------------

  // Case 78     min     max-     min-
  ASSERT_EQ("NotATriangle", triangle(1, 199, 0));
  // Case 79     min     max-     min
  ASSERT_EQ("NotATriangle", triangle(1, 199, 1));
  // Case 80     min     max-     min+
  ASSERT_EQ("NotATriangle", triangle(1, 199, 2));
  // Case 81     min     max-     nom
  ASSERT_EQ("NotATriangle", triangle(1, 199, 102));
  // Case 82     min     max-     max-
  ASSERT_EQ("Isosceles", triangle(1, 199, 199));
  // Case 83     min     max-     max
  ASSERT_EQ("NotATriangle", triangle(1, 199, 200));
  // Case 84     min     max-     max+
  ASSERT_EQ("NotATriangle", triangle(1, 199, 201));
  //  ----------------------------

  // Case 85     min     max     min-
  ASSERT_EQ("NotATriangle", triangle(1, 200, 0));
  // Case 86     min     max     min
  ASSERT_EQ("NotATriangle", triangle(1, 200, 1));
  // Case 87     min     max     min+
  ASSERT_EQ("NotATriangle", triangle(1, 200, 2));
  // Case 88     min     max     nom
  ASSERT_EQ("NotATriangle", triangle(1, 200, 103));
  // Case 89     min     max     max-
  ASSERT_EQ("NotATriangle", triangle(1, 200, 199));
  // Case 90     min     max     max
  ASSERT_EQ("Isosceles", triangle(1, 200, 200));
  // Case 91     min     max     max+
  ASSERT_EQ("NotATriangle", triangle(1, 200, 201));
  //  ----------------------------

  // Case 92     min     max+     min-
  ASSERT_EQ("NotATriangle", triangle(1, 201, 0));
  // Case 93     min     max+     min
  ASSERT_EQ("NotATriangle", triangle(1, 201, 1));
  // Case 94     min     max+     min+
  ASSERT_EQ("NotATriangle", triangle(1, 201, 2));
  // Case 95     min     max+     nom
  ASSERT_EQ("NotATriangle", triangle(1, 201, 104));
  // Case 96     min     max+     max-
  ASSERT_EQ("NotATriangle", triangle(1, 201, 199));
  // Case 97     min     max+     max
  ASSERT_EQ("NotATriangle", triangle(1, 201, 200));
  // Case 98     min     max+     max+
  ASSERT_EQ("NotATriangle", triangle(1, 201, 201));
  //  ----------------------------

  // Case 99     min+     min-     min-
  ASSERT_EQ("NotATriangle", triangle(2, 0, 0));
  // Case 100     min+     min-     min
  ASSERT_EQ("NotATriangle", triangle(2, 0, 1));
  // Case 101     min+     min-     min+
  ASSERT_EQ("NotATriangle", triangle(2, 0, 2));
  // Case 102     min+     min-     nom
  ASSERT_EQ("NotATriangle", triangle(2, 0, 105));
  // Case 103     min+     min-     max-
  ASSERT_EQ("NotATriangle", triangle(2, 0, 199));
  // Case 104     min+     min-     max
  ASSERT_EQ("NotATriangle", triangle(2, 0, 200));
  // Case 105     min+     min-     max+
  ASSERT_EQ("NotATriangle", triangle(2, 0, 201));
  //  ----------------------------

  // Case 106     min+     min     min-
  ASSERT_EQ("NotATriangle", triangle(2, 1, 0));
  // Case 107     min+     min     min
  ASSERT_EQ("NotATriangle", triangle(2, 1, 1));
  // Case 108     min+     min     min+
  ASSERT_EQ("Isosceles", triangle(2, 1, 2));
  // Case 109     min+     min     nom
  ASSERT_EQ("NotATriangle", triangle(2, 1, 106));
  // Case 110     min+     min     max-
  ASSERT_EQ("NotATriangle", triangle(2, 1, 199));
  // Case 111     min+     min     max
  ASSERT_EQ("NotATriangle", triangle(2, 1, 200));
  // Case 112     min+     min     max+
  ASSERT_EQ("NotATriangle", triangle(2, 1, 201));
  //  ----------------------------

  // Case 113     min+     min+     min-
  ASSERT_EQ("NotATriangle", triangle(2, 2, 0));
  // Case 114     min+     min+     min
  ASSERT_EQ("Isosceles", triangle(2, 2, 1));
  // Case 115     min+     min+     min+
  ASSERT_EQ("Equilateral", triangle(2, 2, 2));
  // Case 116     min+     min+     nom
  ASSERT_EQ("NotATriangle", triangle(2, 2, 107));
  // Case 117     min+     min+     max-
  ASSERT_EQ("NotATriangle", triangle(2, 2, 199));
  // Case 118     min+     min+     max
  ASSERT_EQ("NotATriangle", triangle(2, 2, 200));
  // Case 119     min+     min+     max+
  ASSERT_EQ("NotATriangle", triangle(2, 2, 201));
  //  ----------------------------

  // Case 120     min+     nom     min-
  ASSERT_EQ("NotATriangle", triangle(2, 36, 0));
  // Case 121     min+     nom     min
  ASSERT_EQ("NotATriangle", triangle(2, 37, 1));
  // Case 122     min+     nom     min+
  ASSERT_EQ("NotATriangle", triangle(2, 38, 2));
  // Case 123     min+     nom     nom
  ASSERT_EQ("Scalene", triangle(2, 39, 40));
  // Case 124     min+     nom     max-
  ASSERT_EQ("NotATriangle", triangle(2, 41, 199));
  // Case 125     min+     nom     max
  ASSERT_EQ("NotATriangle", triangle(2, 42, 200));
  // Case 126     min+     nom     max+
  ASSERT_EQ("NotATriangle", triangle(2, 43, 201));
  //  ----------------------------

  // Case 127     min+     max-     min-
  ASSERT_EQ("NotATriangle", triangle(2, 199, 0));
  // Case 128     min+     max-     min
  ASSERT_EQ("NotATriangle", triangle(2, 199, 1));
  // Case 129     min+     max-     min+
  ASSERT_EQ("NotATriangle", triangle(2, 199, 2));
  // Case 130     min+     max-     nom
  ASSERT_EQ("NotATriangle", triangle(2, 199, 108));
  // Case 131     min+     max-     max-
  ASSERT_EQ("Isosceles", triangle(2, 199, 199));
  // Case 132     min+     max-     max
  ASSERT_EQ("Scalene", triangle(2, 199, 200));
  // Case 133     min+     max-     max+
  ASSERT_EQ("NotATriangle", triangle(2, 199, 201));
  //  ----------------------------

  // Case 134     min+     max     min-
  ASSERT_EQ("NotATriangle", triangle(2, 200, 0));
  // Case 135     min+     max     min
  ASSERT_EQ("NotATriangle", triangle(2, 200, 1));
  // Case 136     min+     max     min+
  ASSERT_EQ("NotATriangle", triangle(2, 200, 2));
  // Case 137     min+     max     nom
  ASSERT_EQ("NotATriangle", triangle(2, 200, 109));
  // Case 138     min+     max     max-
  ASSERT_EQ("Scalene", triangle(2, 200, 199));
  // Case 139     min+     max     max
  ASSERT_EQ("Isosceles", triangle(2, 200, 200));
  // Case 140     min+     max     max+
  ASSERT_EQ("NotATriangle", triangle(2, 200, 201));
  //  ----------------------------

  // Case 141     min+     max+     min-
  ASSERT_EQ("NotATriangle", triangle(2, 201, 0));
  // Case 142     min+     max+     min
  ASSERT_EQ("NotATriangle", triangle(2, 201, 1));
  // Case 143     min+     max+     min+
  ASSERT_EQ("NotATriangle", triangle(2, 201, 2));
  // Case 144     min+     max+     nom
  ASSERT_EQ("NotATriangle", triangle(2, 201, 110));
  // Case 145     min+     max+     max-
  ASSERT_EQ("NotATriangle", triangle(2, 201, 199));
  // Case 146     min+     max+     max
  ASSERT_EQ("NotATriangle", triangle(2, 201, 200));
  // Case 147     min+     max+     max+
  ASSERT_EQ("NotATriangle", triangle(2, 201, 201));
  //  ----------------------------

  // Case 148     nom     min-     min-
  ASSERT_EQ("NotATriangle", triangle(69, 0, 0));
  // Case 149     nom     min-     min
  ASSERT_EQ("NotATriangle", triangle(420, 0, 1));
  // Case 150     nom     min-     min+
  ASSERT_EQ("NotATriangle", triangle(8008, 0, 2));
  // Case 151     nom     min-     nom
  ASSERT_EQ("NotATriangle", triangle(1, 0, 3));
  // Case 152     nom     min-     max-
  ASSERT_EQ("NotATriangle", triangle(6, 0, 199));
  // Case 153     nom     min-     max
  ASSERT_EQ("NotATriangle", triangle(100, 0, 200));
  // Case 154     nom     min-     max+
  ASSERT_EQ("NotATriangle", triangle(234, 0, 201));
  //  ----------------------------

  // Case 155     nom     min     min-
  ASSERT_EQ("NotATriangle", triangle(0, 1, 0));
  // Case 156     nom     min     min
  ASSERT_EQ("NotATriangle", triangle(5, 1, 1));
  // Case 157     nom     min     min+
  ASSERT_EQ("NotATriangle", triangle(9, 1, 2));
  // Case 158     nom     min     nom
  ASSERT_EQ("NotATriangle", triangle(2, 1, 18));
  // Case 159     nom     min     max-
  ASSERT_EQ("NotATriangle", triangle(150, 1, 199));
  // Case 160     nom     min     max
  ASSERT_EQ("NotATriangle", triangle(150, 1, 200));
  // Case 161     nom     min     max+
  ASSERT_EQ("NotATriangle", triangle(4, 1, 201));
  //  ----------------------------

  // Case 162     nom     min+     min-
  ASSERT_EQ("NotATriangle", triangle(4, 2, 0));
  // Case 163     nom     min+     min
  ASSERT_EQ("NotATriangle", triangle(4, 2, 1));
  // Case 164     nom     min+     min+
  ASSERT_EQ("Isosceles", triangle(1, 2, 2));
  // Case 165     nom     min+     nom
  ASSERT_EQ("NotATriangle", triangle(2, 2, 16));
  // Case 166     nom     min+     max-
  ASSERT_EQ("NotATriangle", triangle(68, 2, 199));
  // Case 167     nom     min+     max
  ASSERT_EQ("NotATriangle", triangle(19, 2, 200));
  // Case 168     nom     min+     max+
  ASSERT_EQ("NotATriangle", triangle(63, 2, 201));
  //  ----------------------------

  // Case 169     nom     nom     min-
  ASSERT_EQ("NotATriangle", triangle(1, 4, 0));
  // Case 170     nom     nom     min
  ASSERT_EQ("Equilateral", triangle(1, 1, 1));
  // Case 171     nom     nom     min+
  ASSERT_EQ("Scalene", triangle(34, 35, 2));
  // Case 172     nom     nom     nom
  ASSERT_EQ("Scalene", triangle(9, 10, 11));
  // Case 173     nom     nom     max-
  ASSERT_EQ("NotATriangle", triangle(3, 100, 199));
  // Case 174     nom     nom     max
  ASSERT_EQ("Scalene", triangle(198, 199, 200));
  // Case 175     nom     nom     max+
  ASSERT_EQ("NotATriangle", triangle(0, 2, 201));
  //  ----------------------------

  // Case 176     nom     max-     min-
  ASSERT_EQ("NotATriangle", triangle(4, 199, 0));
  // Case 177     nom     max-     min
  ASSERT_EQ("NotATriangle", triangle(119, 199, 1));
  // Case 178     nom     max-     min+
  ASSERT_EQ("NotATriangle", triangle(56, 199, 2));
  // Case 179     nom     max-     nom
  ASSERT_EQ("Equilateral", triangle(199, 199, 199));
  // Case 180     nom     max-     max-
  ASSERT_EQ("Isosceles", triangle(100, 199, 199));
  // Case 181     nom     max-     max
  ASSERT_EQ("Scalene", triangle(134, 199, 200));
  // Case 182     nom     max-     max+
  ASSERT_EQ("NotATriangle", triangle(23, 199, 201));
  //  ----------------------------

  // Case 183     nom     max     min-
  ASSERT_EQ("NotATriangle", triangle(55, 200, 0));
  // Case 184     nom     max     min
  ASSERT_EQ("NotATriangle", triangle(56, 200, 1));
  // Case 185     nom     max     min+
  ASSERT_EQ("NotATriangle", triangle(57, 200, 2));
  // Case 186     nom     max     nom
  ASSERT_EQ("NotATriangle", triangle(58, 200, 59));
  // Case 187     nom     max     max-
  ASSERT_EQ("Scalene", triangle(60, 200, 199));
  // Case 188     nom     max     max
  ASSERT_EQ("Isosceles", triangle(61, 200, 200));
  // Case 189     nom     max     max+
  ASSERT_EQ("NotATriangle", triangle(62, 200, 201));
  //  ----------------------------

  // Case 190     nom     max+     min-
  ASSERT_EQ("NotATriangle", triangle(63, 201, 0));
  // Case 191     nom     max+     min
  ASSERT_EQ("NotATriangle", triangle(64, 201, 1));
  // Case 192     nom     max+     min+
  ASSERT_EQ("NotATriangle", triangle(65, 201, 2));
  // Case 193     nom     max+     nom
  ASSERT_EQ("NotATriangle", triangle(66, 201, 111));
  // Case 194     nom     max+     max-
  ASSERT_EQ("NotATriangle", triangle(67, 201, 199));
  // Case 195     nom     max+     max
  ASSERT_EQ("NotATriangle", triangle(68, 201, 200));
  // Case 196     nom     max+     max+
  ASSERT_EQ("NotATriangle", triangle(69, 201, 201));
  //  ----------------------------

  // Case 197     max-     min-     min-
  ASSERT_EQ("NotATriangle", triangle(199, 0, 0));
  // Case 198     max-     min-     min
  ASSERT_EQ("NotATriangle", triangle(199, 0, 1));
  // Case 199     max-     min-     min+
  ASSERT_EQ("NotATriangle", triangle(199, 0, 2));
  // Case 200     max-     min-     nom
  ASSERT_EQ("NotATriangle", triangle(199, 0, 112));
  // Case 201     max-     min-     max-
  ASSERT_EQ("NotATriangle", triangle(199, 0, 199));
  // Case 202     max-     min-     max
  ASSERT_EQ("NotATriangle", triangle(199, 0, 200));
  // Case 203     max-     min-     max+
  ASSERT_EQ("NotATriangle", triangle(199, 0, 201));
  //  ----------------------------

  // Case 204     max-     min     min-
  ASSERT_EQ("NotATriangle", triangle(199, 1, 0));
  // Case 205     max-     min     min
  ASSERT_EQ("NotATriangle", triangle(199, 1, 1));
  // Case 206     max-     min     min+
  ASSERT_EQ("NotATriangle", triangle(199, 1, 2));
  // Case 207     max-     min     nom
  ASSERT_EQ("NotATriangle", triangle(199, 1, 113));
  // Case 208     max-     min     max-
  ASSERT_EQ("Isosceles", triangle(199, 1, 199));
  // Case 209     max-     min     max
  ASSERT_EQ("NotATriangle", triangle(199, 1, 200));
  // Case 210     max-     min     max+
  ASSERT_EQ("NotATriangle", triangle(199, 1, 201));
  //  ----------------------------

  // Case 211     max-     min+     min-
  ASSERT_EQ("NotATriangle", triangle(199, 2, 0));
  // Case 212     max-     min+     min
  ASSERT_EQ("NotATriangle", triangle(199, 2, 1));
  // Case 213     max-     min+     min+
  ASSERT_EQ("NotATriangle", triangle(199, 2, 2));
  // Case 214     max-     min+     nom
  ASSERT_EQ("NotATriangle", triangle(199, 2, 114));
  // Case 215     max-     min+     max-
  ASSERT_EQ("Isosceles", triangle(199, 2, 199));
  // Case 216     max-     min+     max
  ASSERT_EQ("Scalene", triangle(199, 2, 200));
  // Case 217     max-     min+     max+
  ASSERT_EQ("NotATriangle", triangle(199, 2, 201));
  //  ----------------------------

  // Case 218     max-     nom     min-
  ASSERT_EQ("NotATriangle", triangle(199, 43, 0));
  // Case 219     max-     nom     min
  ASSERT_EQ("NotATriangle", triangle(199, 44, 1));
  // Case 220     max-     nom     min+
  ASSERT_EQ("NotATriangle", triangle(199, 45, 2));
  // Case 221     max-     nom     nom
  ASSERT_EQ("NotATriangle", triangle(199, 46, 47));
  // Case 222     max-     nom     max-
  ASSERT_EQ("Isosceles", triangle(199, 48, 199));
  // Case 223     max-     nom     max
  ASSERT_EQ("Scalene", triangle(199, 49, 200));
  // Case 224     max-     nom     max+
  ASSERT_EQ("NotATriangle", triangle(199, 50, 201));
  //  ----------------------------

  // Case 225     max-     max-     min-
  ASSERT_EQ("NotATriangle", triangle(199, 199, 0));
  // Case 226     max-     max-     min
  ASSERT_EQ("Isosceles", triangle(199, 199, 1));
  // Case 227     max-     max-     min+
  ASSERT_EQ("Isosceles", triangle(199, 199, 2));
  // Case 228     max-     max-     nom
  ASSERT_EQ("Isosceles", triangle(199, 199, 115));
  // Case 229     max-     max-     max-
  ASSERT_EQ("Equilateral", triangle(199, 199, 199));
  // Case 230     max-     max-     max
  ASSERT_EQ("Isosceles", triangle(199, 199, 200));
  // Case 231     max-     max-     max+
  ASSERT_EQ("NotATriangle", triangle(199, 199, 201));
  //  ----------------------------

  // Case 232     max-     max     min-
  ASSERT_EQ("NotATriangle", triangle(199, 200, 0));
  // Case 233     max-     max     min
  ASSERT_EQ("NotATriangle", triangle(199, 200, 1));
  // Case 234     max-     max     min+
  ASSERT_EQ("Scalene", triangle(199, 200, 2));
  // Case 235     max-     max     nom
  ASSERT_EQ("Scalene", triangle(199, 200, 116));
  // Case 236     max-     max     max-
  ASSERT_EQ("Isosceles", triangle(199, 200, 199));
  // Case 237     max-     max     max
  ASSERT_EQ("Isosceles", triangle(199, 200, 200));
  // Case 238     max-     max     max+
  ASSERT_EQ("NotATriangle", triangle(199, 200, 201));
  //  ----------------------------

  // Case 239     max-     max+     min-
  ASSERT_EQ("NotATriangle", triangle(199, 201, 0));
  // Case 240     max-     max+     min
  ASSERT_EQ("NotATriangle", triangle(199, 201, 1));
  // Case 241     max-     max+     min+
  ASSERT_EQ("NotATriangle", triangle(199, 201, 2));
  // Case 242     max-     max+     nom
  ASSERT_EQ("NotATriangle", triangle(199, 201, 118));
  // Case 243     max-     max+     max-
  ASSERT_EQ("NotATriangle", triangle(199, 201, 199));
  // Case 244     max-     max+     max
  ASSERT_EQ("NotATriangle", triangle(199, 201, 200));
  // Case 245     max-     max+     max+
  ASSERT_EQ("NotATriangle", triangle(199, 201, 201));
  //  ----------------------------

  // Case 246     max     min-     min-
  ASSERT_EQ("NotATriangle", triangle(200, 0, 0));
  // Case 247     max     min-     min
  ASSERT_EQ("NotATriangle", triangle(200, 0, 1));
  // Case 248     max     min-     min+
  ASSERT_EQ("NotATriangle", triangle(200, 0, 2));
  // Case 249     max     min-     nom
  ASSERT_EQ("NotATriangle", triangle(200, 0, 119));
  // Case 250     max     min-     max-
  ASSERT_EQ("NotATriangle", triangle(200, 0, 199));
  // Case 251     max     min-     max
  ASSERT_EQ("NotATriangle", triangle(200, 0, 200));
  // Case 252     max     min-     max+
  ASSERT_EQ("NotATriangle", triangle(200, 0, 201));
  //  ----------------------------

  // Case 253     max     min     min-
  ASSERT_EQ("NotATriangle", triangle(200, 1, 0));
  // Case 254     max     min     min
  ASSERT_EQ("NotATriangle", triangle(200, 1, 1));
  // Case 255     max     min     min+
  ASSERT_EQ("NotATriangle", triangle(200, 1, 2));
  // Case 256     max     min     nom
  ASSERT_EQ("NotATriangle", triangle(200, 1, 120));
  // Case 257     max     min     max-
  ASSERT_EQ("NotATriangle", triangle(200, 1, 199));
  // Case 258     max     min     max
  ASSERT_EQ("Isosceles", triangle(200, 1, 200));
  // Case 259     max     min     max+
  ASSERT_EQ("NotATriangle", triangle(200, 1, 201));
  //  ----------------------------

  // Case 260     max     min+     min-
  ASSERT_EQ("NotATriangle", triangle(200, 2, 0));
  // Case 261     max     min+     min
  ASSERT_EQ("NotATriangle", triangle(200, 2, 1));
  // Case 262     max     min+     min+
  ASSERT_EQ("NotATriangle", triangle(200, 2, 2));
  // Case 263     max     min+     nom
  ASSERT_EQ("NotATriangle", triangle(200, 2, 121));
  // Case 264     max     min+     max-
  ASSERT_EQ("Scalene", triangle(200, 2, 199));
  // Case 265     max     min+     max
  ASSERT_EQ("Isosceles", triangle(200, 2, 200));
  // Case 266     max     min+     max+
  ASSERT_EQ("NotATriangle", triangle(200, 2, 201));
  //  ----------------------------

  // Case 267     max     nom     min-
  ASSERT_EQ("NotATriangle", triangle(200, 51, 0));
  // Case 268     max     nom     min
  ASSERT_EQ("NotATriangle", triangle(200, 52, 1));
  // Case 269     max     nom     min+
  ASSERT_EQ("NotATriangle", triangle(200, 53, 2));
  // Case 270     max     nom     nom
  ASSERT_EQ("NotATriangle", triangle(200, 54, 55));
  // Case 271     max     nom     max-
  ASSERT_EQ("Scalene", triangle(200, 56, 199));
  // Case 272     max     nom     max
  ASSERT_EQ("Isosceles", triangle(200, 57, 200));
  // Case 273     max     nom     max+
  ASSERT_EQ("NotATriangle", triangle(200, 58, 201));
  //  ----------------------------

  // Case 274     max     max-     min-
  ASSERT_EQ("NotATriangle", triangle(200, 199, 0));
  // Case 275     max     max-     min
  ASSERT_EQ("NotATriangle", triangle(200, 199, 1));
  // Case 276     max     max-     min+
  ASSERT_EQ("Scalene", triangle(200, 199, 2));
  // Case 277     max     max-     nom
  ASSERT_EQ("Scalene", triangle(200, 199, 122));
  // Case 278     max     max-     max-
  ASSERT_EQ("Isosceles", triangle(200, 199, 199));
  // Case 279     max     max-     max
  ASSERT_EQ("Isosceles", triangle(200, 199, 200));
  // Case 280     max     max-     max+
  ASSERT_EQ("NotATriangle", triangle(200, 199, 201));
  //  ----------------------------

  // Case 281     max     max     min-
  ASSERT_EQ("NotATriangle", triangle(200, 200, 0));
  // Case 282     max     max     min
  ASSERT_EQ("Isosceles", triangle(200, 200, 1));
  // Case 283     max     max     min+
  ASSERT_EQ("Isosceles", triangle(200, 200, 2));
  // Case 284     max     max     nom
  ASSERT_EQ("Isosceles", triangle(200, 200, 123));
  // Case 285     max     max     max-
  ASSERT_EQ("Isosceles", triangle(200, 200, 199));
  // Case 286     max     max     max
  ASSERT_EQ("Equilateral", triangle(200, 200, 200));
  // Case 287     max     max     max+
  ASSERT_EQ("NotATriangle", triangle(200, 200, 201));
  //  ----------------------------

  // Case 288     max     max+     min-
  ASSERT_EQ("NotATriangle", triangle(200, 201, 0));
  // Case 289     max     max+     min
  ASSERT_EQ("NotATriangle", triangle(200, 201, 1));
  // Case 290     max     max+     min+
  ASSERT_EQ("NotATriangle", triangle(200, 201, 2));
  // Case 291     max     max+     nom
  ASSERT_EQ("NotATriangle", triangle(200, 201, 124));
  // Case 292     max     max+     max-
  ASSERT_EQ("NotATriangle", triangle(200, 201, 199));
  // Case 293     max     max+     max
  ASSERT_EQ("NotATriangle", triangle(200, 201, 200));
  // Case 294     max     max+     max+
  ASSERT_EQ("NotATriangle", triangle(200, 201, 201));
  //  ----------------------------

  // Case 295     max+     min-     min-
  ASSERT_EQ("NotATriangle", triangle(201, 0, 0));
  // Case 296     max+     min-     min
  ASSERT_EQ("NotATriangle", triangle(201, 0, 1));
  // Case 297     max+     min-     min+
  ASSERT_EQ("NotATriangle", triangle(201, 0, 2));
  // Case 298     max+     min-     nom
  ASSERT_EQ("NotATriangle", triangle(201, 0, 105));
  // Case 299     max+     min-     max-
  ASSERT_EQ("NotATriangle", triangle(201, 0, 199));
  // Case 300     max+     min-     max
  ASSERT_EQ("NotATriangle", triangle(201, 0, 200));
  // Case 301     max+     min-     max+
  ASSERT_EQ("NotATriangle", triangle(201, 0, 201));
  //  ----------------------------

  // Case 302     max+     min     min-
  ASSERT_EQ("NotATriangle", triangle(201, 1, 0));
  // Case 303     max+     min     min
  ASSERT_EQ("NotATriangle", triangle(201, 1, 1));
  // Case 304     max+     min     min+
  ASSERT_EQ("NotATriangle", triangle(201, 1, 2));
  // Case 305     max+     min     nom
  ASSERT_EQ("NotATriangle", triangle(201, 1, 106));
  // Case 306     max+     min     max-
  ASSERT_EQ("NotATriangle", triangle(201, 1, 199));
  // Case 307     max+     min     max
  ASSERT_EQ("NotATriangle", triangle(201, 1, 200));
  // Case 308     max+     min     max+
  ASSERT_EQ("NotATriangle", triangle(201, 1, 201));
  //  ----------------------------

  // Case 309     max+     min+     min-
  ASSERT_EQ("NotATriangle", triangle(201, 2, 0));
  // Case 310     max+     min+     min
  ASSERT_EQ("NotATriangle", triangle(201, 2, 1));
  // Case 311     max+     min+     min+
  ASSERT_EQ("NotATriangle", triangle(201, 2, 2));
  // Case 312     max+     min+     nom
  ASSERT_EQ("NotATriangle", triangle(201, 2, 107));
  // Case 313     max+     min+     max-
  ASSERT_EQ("NotATriangle", triangle(201, 2, 199));
  // Case 314     max+     min+     max
  ASSERT_EQ("NotATriangle", triangle(201, 2, 200));
  // Case 315     max+     min+     max+
  ASSERT_EQ("NotATriangle", triangle(201, 2, 201));
  //  ----------------------------

  // Case 316     max+     nom     min-
  ASSERT_EQ("NotATriangle", triangle(201, 59, 0));
  // Case 317     max+     nom     min
  ASSERT_EQ("NotATriangle", triangle(201, 60, 1));
  // Case 318     max+     nom     min+
  ASSERT_EQ("NotATriangle", triangle(201, 70, 2));
  // Case 319     max+     nom     nom
  ASSERT_EQ("NotATriangle", triangle(201, 71, 72));
  // Case 320     max+     nom     max-
  ASSERT_EQ("NotATriangle", triangle(201, 73, 199));
  // Case 321     max+     nom     max
  ASSERT_EQ("NotATriangle", triangle(201, 74, 200));
  // Case 322     max+     nom     max+
  ASSERT_EQ("NotATriangle", triangle(201, 75, 201));
  //  ----------------------------

  // Case 323     max+     max-     min-
  ASSERT_EQ("NotATriangle", triangle(201, 199, 0));
  // Case 324     max+     max-     min
  ASSERT_EQ("NotATriangle", triangle(201, 199, 1));
  // Case 325     max+     max-     min+
  ASSERT_EQ("NotATriangle", triangle(201, 199, 2));
  // Case 326     max+     max-     nom
  ASSERT_EQ("NotATriangle", triangle(201, 199, 74));
  // Case 327     max+     max-     max-
  ASSERT_EQ("NotATriangle", triangle(201, 199, 199));
  // Case 328     max+     max-     max
  ASSERT_EQ("NotATriangle", triangle(201, 199, 200));
  // Case 329     max+     max-     max+
  ASSERT_EQ("NotATriangle", triangle(201, 199, 201));
  //  ----------------------------

  // Case 330     max+     max     min-
  ASSERT_EQ("NotATriangle", triangle(201, 200, 0));
  // Case 331     max+     max     min
  ASSERT_EQ("NotATriangle", triangle(201, 200, 1));
  // Case 332     max+     max     min+
  ASSERT_EQ("NotATriangle", triangle(201, 200, 2));
  // Case 333     max+     max     nom
  ASSERT_EQ("NotATriangle", triangle(201, 200, 75));
  // Case 334     max+     max     max-
  ASSERT_EQ("NotATriangle", triangle(201, 200, 199));
  // Case 335     max+     max     max
  ASSERT_EQ("NotATriangle", triangle(201, 200, 200));
  // Case 336     max+     max     max+
  ASSERT_EQ("NotATriangle", triangle(201, 200, 201));
  //  ----------------------------

  // Case 337     max+     max+     min-
  ASSERT_EQ("NotATriangle", triangle(201, 201, 0));
  // Case 338     max+     max+     min
  ASSERT_EQ("NotATriangle", triangle(201, 201, 1));
  // Case 339     max+     max+     min+
  ASSERT_EQ("NotATriangle", triangle(201, 201, 2));
  // Case 340     max+     max+     nom
  ASSERT_EQ("NotATriangle", triangle(201, 201, 76));
  // Case 341     max+     max+     max-
  ASSERT_EQ("NotATriangle", triangle(201, 201, 199));
  // Case 342     max+     max+     max
  ASSERT_EQ("NotATriangle", triangle(201, 201, 200));
  // Case 343     max+     max+     max+
  ASSERT_EQ("NotATriangle", triangle(201, 201, 201));
}

TEST(TriangleTests, Special_VT) {
  //  Case 1  min    nom   nom 
  ASSERT_EQ("Equilateral", triangle(1, 1, 1));
  //  Case 2  min+   nom   nom
  ASSERT_EQ("NotATriangle", triangle(2, 1, 1));
  //  Case 3  max    nom   nom
  ASSERT_EQ("NotATriangle", triangle(200, 1, 1));
  //  Case 4  max-   nom   nom
  ASSERT_EQ("NotATriangle", triangle(199, 1, 1));
  //  ---------------------- 
  //  Case 5  nom    min   nom
  ASSERT_EQ("NotATriangle", triangle(5, 1, 3));
  //  Case 6  nom    min+  nom
  ASSERT_EQ("NotATriangle", triangle(5, 2, 3));
  //  Case 7  nom    max   nom
  ASSERT_EQ("NotATriangle", triangle(11, 200, 91));
  //  Case 8  nom    max-  nom
  ASSERT_EQ("NotATriangle", triangle(11, 199, 91));
  //  ---------------------- 
  //  Case 9  nom    nom   min 
  ASSERT_EQ("NotATriangle", triangle(4, 8, 1));
  //  Case 10 nom    nom   min+ 
  ASSERT_EQ("NotATriangle", triangle(16, 30, 2));
  //  Case 11 nom    nom   max
  ASSERT_EQ("Scalene", triangle(45, 160, 200));
  //  Case 12 nom    nom   max-
  ASSERT_EQ("Equilateral", triangle(199, 199, 199));
  
  // Case n...  nom     nom     nom
  // Specifics?
  ASSERT_EQ("Scalene", triangle(35, 42, 23));
  
  ASSERT_EQ("Isosceles", triangle(111, 111, 57));

  ASSERT_EQ("Equilateral", triangle(50, 50, 50));

  ASSERT_EQ("Scalene", triangle(123, 50, 167));

  ASSERT_EQ("NotATriangle", triangle(45, 93, 45));
}

TEST(TriangleTests, Random_Testing) {
  // Do a loop for grabbing random values
  // a, b, c and get the return string 
  // from the triangle func for the 
  // expected output
  for (int i = 0; i < 20; i++) {
    // random number a
    int a = rand() % 200 + 1;
    // random number b
    int b = rand() % 200 + 1;
    // random number c
    int c = rand() % 200 + 1;
    std::string str = triangle(a, b, c);
    std::cout << "\n" << a << ", " << b << ", "
      << c << " should equal " << str << std::endl;
    ASSERT_EQ(str, triangle(a, b, c));
  }
}
