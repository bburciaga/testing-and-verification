# lab8

### Input Events

- I1  depress wireless button
- I2  depress door button
- I3  depress digit on keypad
- I4  end of down track hit
- I5  end of up track hit
- I6  obsticle hit
- I7  laser hit
- I8  end timer hit

### Output Events

- O1  start drive motor down
- O2  start drive motor up
- O3  stop drive motor
- O4  stop drive motor part-way
- O5  door continues closing
- O6  door cuntinues opening
- O7  lamp turns on
- O8  lamp turns off
- O9  laser turns on
- O10  laser turns off
- O11 start 30s timer
- O12 "----"
- O13 "---\*"
- O14 "--\*\*"
- O15 "-\*\*\*"
- O16 "\*\*\*\*"


### Sequence Events

- S1: Door is closed
- S2: Door is opened
- S3: Door is opening
- S4: Door is closing
- S5: Door halts mid-way
- S6: Counting Down
- S7: Updating Keypad Display

## Extended Essential Use-Cases

### Wireless Button

| Column 1                     | Column 2                                         | 
|------------------------------|--------------------------------------------------|
|**Use Case Name**             |Door Open: Wireless Button                        |
|**Use Case ID**               |DO_01                                             |
|**Description**               |A user pushes the wireless button to open the door|
|**Pre-Condition**             |S1: Door is closed                                |
|**Input Events**              |**Output Events**                                 |
|I1: Depress Wireless Button   |                                                  |
|                              |O2: Start driver moter up                         |
|                              |O7: Lamp turns on                                 |
|                              |O9: Laser turns on                                |
|I5: End of up track hit       |                                                  |
|                              |O3: Stop drive motor                              |
|**Post-Condition**            |S2: Door is open                                  |

| Column 1                     | Column 2                                         | 
|------------------------------|--------------------------------------------------|
|**Use Case Name**             |Laser Sensor Triggered: Wireless Button           |
|**Use Case ID**               |LST_01                                            |
|**Description**               |An object triggers the laser sensor while the door is closing|
|**Pre-Condition**             |S2: Door is open                                  |
|**Input Events**              |**Output Events**                                 |
|I1: Depress Wireless Button   |                                                  |
|                              |O1: Start driver motor down                       |
|I7: Laser hit                 |                                                  |
|                              |O4: Door stops part-way                           |
|I1: Depress Wireless Button   |                                                  |
|                              |O6: Door continues closing                        |
|I4: End of down track hit     |                                                  |
|                              |O11: Start 30s Timer                              |
|I8: End Timer Hit             |                                                  |
|                              |O8: Lamp Turns Off                                |
|                              |O10: Laser Turns Off                              |
|**Post-Condition**            |S1: Door is closed                                |

| Column 1                     | Column 2                                         | 
|------------------------------|--------------------------------------------------|
|**Use Case Name**             |Pressure Sensor Triggered: Wireleess Button|
|**Use Case ID**               |PST_01|
|**Description**               |The laser sensor was not triggered and the door closes until it touches the object via the wireless button|
|**Pre-Condition**             |S2: Door is open              |
|**Input Events**              |**Output Events**             |
|I1: Depress Wireless Button   |                              |
|                              |O1: Start driver motor down   |
|I6: Obsticle hit              |                              |
|                              |O4: Door stops part-way       |
|I1: Depress Wireless Button   |                              |
|                              |O6: Door continues closing    |
|I4: End of down track hit     |                        |
|                              |O11: Start 30s Timer    |
|I8: End Timer Hit             |                        |
|                              |O8: Lamp Turns Off      |
|                              |O10: Laser Turns Off    |
|**Post-Condition**            |S1: Door is closed      |

| Column 1                     | Column 2                                         | 
|------------------------------|--------------------------------------------------|
|**Use Case Name**             |Interrupt: Wireleess Button|
|**Use Case ID**               |I_01|
|**Description**               |Wireless button is pushed to interrupt closing process|
|**Pre-Condition**             |S2: Door is open|
|**Input Events**              |**Output Events**|
|I1: Depress Wireless Button| |
|                              |O1: Start driver motor down|
|I1: Depress Wireless Button| |
|                              |O4: Door stops part-way|
|I1: Depress Wireless Button| |
|                              |O6: Door continues closing|
|I4: End of down track hit  |   |
|                              |O11: Start 30s Timer|
|I8: End Timer Hit    | |
|                              |O8: Lamp Turns Off|
|                              |O10: Laser Turns Off|
|**Post-Condition**            |S1: Door is closed|

| Column 1                     | Column 2                                         | 
|------------------------------|--------------------------------------------------|
|**Use Case Name**             |Door Closes: Wireleess Button|
|**Use Case ID**               |DC_01|
|**Description**               |Door closes all the way with no interruptiosn|
|**Pre-Condition**             |S2: Door is open|
|**Input Events**              |**Output Events**|
|I1: Depress Wireless Button  | |
|                              |O1: Start driver motor down|
|I4: End of down track hit  | |
|                              |O11: Start 30s Timer|
|I8: End Timer Hit    |   |
|                              |O8: Lamp Turns Off|
|                              |O10: Laser Turns Off|
|**Post-Condition**            |S1: Door is closed|

### Door Button

| Column 1                     | Column 2                                         | 
|------------------------------|--------------------------------------------------|
|**Use Case Name**             |Door Open: Door Button|
|**Use Case ID**               |DO_02|
|**Description**               |A user pushes the door button to open the door|
|**Pre-Condition**             |S1: Door is closed|
|**Input Events**              |**Output Events**|
|I2: Depress Door Button  ||
|                              |O2: Start driver moter up|
|                              |O7: Lamp turns on|
|                              |O9: Laser turns on|
|I5: End of up track hit  ||
|                              |O3: Stop drive motor|
|**Post-Condition**            |S2: Door is open|

| Column 1                     | Column 2                                         | 
|------------------------------|--------------------------------------------------|
|**Use Case Name**             |Laser Sensor Triggered: Door Button|
|**Use Case ID**               |LST_02|
|**Description**               |An object triggers the laser sensor while the door is closing|
|**Pre-Condition**             |S2: Door is open|
|**Input Events**              |**Output Events**|
|I2: Depress Door Button    ||
|                              |O1: Start driver motor down|
|I7: Laser hit    ||
|                              |O4: Door stops part-way|
|I2: Depress Door Button  ||
|                              |O6: Door continues closing|
|I4: End of down track hit  ||
|                              |O11: Start 30s Timer|
|I8: End Timer Hit    ||
|                              |O8: Lamp Turns Off|
|                              |O10: Laser Turns Off|
|**Post-Condition**            |S1: Door is closed|

| Column 1                     | Column 2                                         | 
|------------------------------|--------------------------------------------------|
|**Use Case Name**             |Pressure Sensor Triggered: Door Button|
|**Use Case ID**               |PST_02|
|**Description**               |The laser sensor was not triggered and the door closes until it touches the object via the door button|
|**Pre-Condition**             |S2: Door is open|
|**Input Events**              |**Output Events**|
|I2: Depress Door Button||
|                              |O1: Start driver motor down|
|I6: Obsticle hit||
|                              |O4: Door stops part-way|
|I2: Depress Door Button||
|                              |O6: Door continues closing|
|I4: End of down track hit||
|                              |O11: Start 30s Timer|
|I8: End Timer Hit||
|                              |O8: Lamp Turns Off|
|                              |O10: Laser Turns Off|
|**Post-Condition**            |S1: Door is closed|

| Column 1                     | Column 2                                         | 
|------------------------------|--------------------------------------------------|
|**Use Case Name**             |Interrupt: Door Button|
|**Use Case ID**               |I_02|
|**Description**               |Door button is pushed to interrupt closing process|
|**Pre-Condition**             |S2: Door is open|
|**Input Events**              |**Output Events**|
|I2: Depress Door Button||
|                              |O1: Start driver motor down|
|I2: Depress Door Button||
|                              |O4: Door stops part-way|
|I2: Depress Door Button||
|                              |O6: Door continues closing|
|I4: End of down track hit||
|                              |O11: Start 30s Timer|
|I8: End Timer Hit||
|                              |O8: Lamp Turns Off|
|                              |O10: Laser Turns Off|
|**Post-Condition**            |S1: Door is closed|

| Column 1                     | Column 2                                         | 
|------------------------------|--------------------------------------------------|
|**Use Case Name**             |Door Closes: Door Button|
|**Use Case ID**               |DC_02|
|**Description**               |Door closes all the way with no interruptiosn|
|**Pre-Condition**             |S2: Door is open|
|**Input Events**              |**Output Events**|
|I2: Depress Door Button||
|                              |O1: Start driver motor down|
|I4: End of down track hit||
|                              |O11: Start 30s Timer|
|I8: End Timer Hit||
|                              |O8: Lamp Turns Off|
|                              |O10: Laser Turns Off|
|**Post-Condition**            |S1: Door is closed|

### Digit Keypad

| Column 1                     | Column 2                                         | 
|------------------------------|--------------------------------------------------|
|**Use Case Name**             |Door Open: Digit Keypad
|**Use Case ID**               |DO_03
|**Description**               |A user pushes the door button to open the door
|**Pre-Condition**             |S1: Door is closed
|**Input Events**              |**Output Events**
|I3: Depress digit of keypad||
|                              |O12: "----"|
|I3: Depress digit of keypad||
|                              |O13: "---\*"|
|I3: Depress digit of keypad||
|                              |O14: "--\*\*"|
|I3: Depress digit of keypad||
|                              |O15: "-\*\*\*"|
|I3: Depress digit of keypad||
|                              |O16: "\*\*\*\*"|
|                              |O2: Start Drive Motor Up|
|I5: End of up track hit||
|                              |O3: Stop drive motor|
|**Post-Condition**            |S2: Door is open|

| Column 1                     | Column 2                                         | 
|------------------------------|--------------------------------------------------|
|**Use Case Name**             |Laser Sensor Triggered: Digit Keypad|
|**Use Case ID**               |LST_03|
|**Description**               |An object triggers the laser sensor while the door is closing|
|**Pre-Condition**             |S2: Door is open|
|**Input Events**              |**Output Events**|
|I3: Depress Digit Keypad||
|                              |O1: Start driver motor down|
|I7: Laser hit||
|                              |O4: Door stops part-way|
|I3: Depress Digit Keypad||
|                              |O6: Door continues closing|
|I4: End of down track hit||
|                              |O11: Start 30s Timer|
|I8: End Timer Hit||
|                              |O8: Lamp Turns Off|
|                              |O10: Laser Turns Off|
|**Post-Condition**            |S1: Door is closed|

| Column 1                     | Column 2                                         | 
|------------------------------|--------------------------------------------------|
|**Use Case Name**             |Pressure Sensor Triggered: Digit Keypad|
|**Use Case ID**               |PST_03|
|**Description**               |The laser sensor was not triggered and the door closes until it touches the object via the digit keypad|
|**Pre-Condition**             |S2: Door is open|
|**Input Events**              |**Output Events**|
|I3: Depress Digit Keypad||
|                              |O1: Start driver motor down|
|I6: Obsticle hit||
|                              |O4: Door stops part-way|
|I3: Depress Digit Keypad||
|                              |O6: Door continues closing|
|I4: End of down track hit||
|                              |O11: Start 30s Timer|
|I8: End Timer Hit||
|                              |O8: Lamp Turns Off|
|                              |O10: Laser Turns Off|
|**Post-Condition**            |S1: Door is closed|

| Column 1                     | Column 2                                         | 
|------------------------------|--------------------------------------------------|
|**Use Case Name**             |Interrupt: Digit Keypad|
|**Use Case ID**               |I_03|
|**Description**               |Door button is pushed to interrupt closing process|
|**Pre-Condition**             |S2: Door is open|
|**Input Events**              |**Output Events**|
|I3: Depress Digit Keypad||
|                              |O1: Start driver motor down|
|I3: Depress Digit Keypad||
|                              |O4: Door stops part-way|
|I3: Depress Digit Keypad||
|                              |O6: Door continues closing|
|I4: End of down track hit||
|                              |O11: Start 30s Timer|
|I8: End Timer Hit||
|                              |O8: Lamp Turns Off|
|                              |O10: Laser Turns Off|
|**Post-Condition**            |S1: Door is closed|

| Column 1                     | Column 2                                         | 
|------------------------------|--------------------------------------------------|
|**Use Case Name**             |Door Closes: Digit Keypad|
|**Use Case ID**               |DC_03|
|**Description**               |Door closes all the way with no interruptiosn|
|**Pre-Condition**             |S2: Door is open|
|**Input Events**              |**Output Events**|
|I3: Depress Digit Keypad||
|                              |O1: Start driver motor down|
|I4: End of down track hit||
|                              |O11: Start 30s Timer|
|I8: End Timer Hit||
|                              |O8: Lamp Turns Off|
|                              |O10: Laser Turns Off|
|**Post-Condition**            |S1: Door is closed|

