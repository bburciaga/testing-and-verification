/**
 * @file tests.cpp
 *
 * @brief DT tests to run the nextDate function to ensure it is working
 *
 * @author Brian Burciaga T00566000
 * Contact: burciagab16@mytru.ca
 * 
 */

#include <iostream>
#include <string>
#include <gtest/gtest.h>
#include "nextDate.hpp"

//  c1:   1812 <= y <= 2012   is valid year?
//  c2:   1 <= m <= 12        is valid month?
//  c3:   1 <= d <= 31        is valid day?
//  d1:   1 <= d <= 27        any day
//  d2:   d == 28
//  d3:   d == 29
//  d4:   d == 30
//  d5:   d == 31
//  m1:   m == 2
//  m2:   month has 30 days
//  m3:   month has 31 days
//  y1:   y is any year
//  y2:   y is a leap year

//  a1: Not a Date
//  a2: Any Next Date
TEST(NextDateTests, Decision_Table){
//  c1  F   T   T   T   T   T   T   T   T   T   T   T   T
//  c2  -   F   T   T   T   T   T   T   T   T   T   T   T
//  c3  -   -   F   T   T   T   T   T   T   T   T   T   T
//  d1  -   -   -   T   T   -   -   -   -   -   -   -   -
//  d2  -   -   -   -   -   T   T   -   -   -   -   -   -
//  d3  -   -   -   -   -   -   -   T   T   -   -   -   -
//  d4  -   -   -   -   -   -   -   -   -   T   T   -   -
//  d5  -   -   -   -   -   -   -   -   -   -   -   T   T
//  m1  -   -   -   T   T   T   T   T   T   T   T   T   T
//  m2  -   -   -   -   -   -   -   -   -   -   -   -   -
//  m3  -   -   -   -   -   -   -   -   -   -   -   -   -
//  y1  -   -   -   T   F   T   F   T   F   T   F   T   F
//  y2  -   -   -   F   T   F   T   F   T   F   T   F   T
//  -----------------------------------------------------
//  a1  X   X   X   -   -   -   -   X   -   X   X   X   X
//  a2  -   -   -   X   X   X   X   -   X   -   -   -   -
  ASSERT_EQ("Not a Date", nextDate(1, 2, 1800));
  ASSERT_EQ("Not a Date", nextDate(3, 0, 1900));
  ASSERT_EQ("Not a Date", nextDate(34, 4, 2000));
  // m == 2
  ASSERT_NE("Not a Date", nextDate(1, 2, 1813));
  ASSERT_NE("Not a Date", nextDate(2, 2, 1816));
  ASSERT_NE("Not a Date", nextDate(28, 2, 1817));
  ASSERT_NE("Not a Date", nextDate(28, 2, 1820));
  ASSERT_EQ("Not a Date", nextDate(29, 2, 1821));
  ASSERT_NE("Not a Date", nextDate(29, 2, 1824));
  ASSERT_EQ("Not a Date", nextDate(30, 2, 1825));
  ASSERT_EQ("Not a Date", nextDate(30, 2, 1828));
  ASSERT_EQ("Not a Date", nextDate(31, 2, 1829));
  ASSERT_EQ("Not a Date", nextDate(31, 2, 1832));
//  =====================================================
//  c1  T   T   T   T   T   T   T   T   T   T
//  c2  T   T   T   T   T   T   T   T   T   T
//  c3  T   T   T   T   T   T   T   T   T   T
//  d1  T   T   -   -   -   -   -   -   -   -
//  d2  -   -   T   T   -   -   -   -   -   -
//  d3  -   -   -   -   T   T   -   -   -   -
//  d4  -   -   -   -   -   -   T   T   -   -
//  d5  -   -   -   -   -   -   -   -   T   T
//  m1  -   -   -   -   -   -   -   -   -   -
//  m2  T   T   T   T   T   T   T   T   T   T
//  m3  -   -   -   -   -   -   -   -   -   -
//  y1  T   F   T   F   T   F   T   F   T   F
//  y2  F   T   F   T   F   T   F   T   F   T
//  -----------------------------------------------------
//  a1  -   -   -   -   -   -   -   -   X   X
//  a2  X   X   X   X   X   X   X   X   -   -
  // m has 30 days
  ASSERT_NE("Not a Date", nextDate(3, 4, 1833));
  ASSERT_NE("Not a Date", nextDate(4, 6, 1836));
  ASSERT_NE("Not a Date", nextDate(28, 9, 1837));
  ASSERT_NE("Not a Date", nextDate(28, 11, 1840));
  ASSERT_NE("Not a Date", nextDate(29, 4, 1841));
  ASSERT_NE("Not a Date", nextDate(29, 6, 1844));
  ASSERT_NE("Not a Date", nextDate(30, 9, 1845));
  ASSERT_NE("Not a Date", nextDate(30, 11, 1848));
  ASSERT_EQ("Not a Date", nextDate(31, 4, 1849));
  ASSERT_EQ("Not a Date", nextDate(31, 6, 1852));
//  =====================================================
//  c1  T   T   T   T   T   T   T   T   T   T
//  c2  T   T   T   T   T   T   T   T   T   T
//  c3  T   T   T   T   T   T   T   T   T   T
//  d1  T   T   -   -   -   -   -   -   -   -
//  d2  -   -   T   T   -   -   -   -   -   -
//  d3  -   -   -   -   T   T   -   -   -   -
//  d4  -   -   -   -   -   -   T   T   -   -
//  d5  -   -   -   -   -   -   -   -   T   T
//  m1  -   -   -   -   -   -   -   -   -   -
//  m2  -   -   -   -   -   -   -   -   -   -
//  m3  T   T   T   T   T   T   T   T   T   T
//  y1  T   F   T   F   T   F   T   F   T   F
//  y2  F   T   F   T   F   T   F   T   F   T
//  -----------------------------------------------------
//  a1  -   -   -   -   -   -   -   -   -   -
//  a2  X   X   X   X   X   X   X   X   X   X
  // m has 31 days
  ASSERT_NE("Not a Date", nextDate(27, 1, 1853));
  ASSERT_NE("Not a Date", nextDate(26, 3, 1856));
  ASSERT_NE("Not a Date", nextDate(28, 5, 1857));
  ASSERT_NE("Not a Date", nextDate(28, 7, 1860));
  ASSERT_NE("Not a Date", nextDate(29, 8, 1864));
  ASSERT_NE("Not a Date", nextDate(29, 10, 1865));
  ASSERT_NE("Not a Date", nextDate(30, 12, 1868));
  ASSERT_NE("Not a Date", nextDate(30, 1, 1872));
  ASSERT_NE("Not a Date", nextDate(31, 3, 1873));
  ASSERT_NE("Not a Date", nextDate(31, 5, 1876));
}

//  c1:   1812 <= y <= 2012   is valid year?
//  c2:   1 <= m <= 12        is valid month?
//  c3:   1 <= d <= 31        is valid day?
//  d1:   1 <= d <= 27        any day
//  d2:   d == 28
//  d3:   d == 29
//  d4:   d == 30
//  d5:   d == 31
//  m1:   m == 2
//  m2:   month has 30 days
//  m3:   month has 31 days
//  m4:   m == 12
//  y1:   y is any year
//  y2:   y is a leap year
//  y3:   y == 2000
//  y4:   y == 1900

//  a1: Not a Date
//  a2: increment day
//  a3: reset day
//  a4: increment month
//  a5: reset month
//  a6: increment year
TEST(NextDateTests, Extended_Entry_Decision_Table){
//  c1  F   T   T   T   T   T   T   T   T   T   T   T   T   T   T   T   T   T   T   T   T   T   T
//  c2  -   F   T   T   T   T   T   T   T   T   T   T   T   T   T   T   T   T   T   T   T   T   T
//  c3  -   -   F   T   T   T   T   T   T   T   T   T   T   T   T   T   T   T   T   T   T   T   T
//  d1  -   -   -   T   T   T   T   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -
//  d2  -   -   -   -   -   -   -   T   T   T   T   -   -   -   -   -   -   -   -   -   -   -   -
//  d3  -   -   -   -   -   -   -   -   -   -   -   T   T   T   T   -   -   -   -   -   -   -   -
//  d4  -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   T   T   T   T   -   -   -   -
//  d5  -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   T   T   T   T
//  m1  -   -   -   T   T   T   T   T   T   T   T   T   T   T   T   T   T   T   T   T   T   T   T
//  m2  -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -
//  m3  -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -
//  m4  -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -
//  y1  -   -   -   T   F   F   F   T   F   F   F   T   F   F   F   T   F   F   F   T   F   F   F
//  y2  -   -   -   F   T   F   F   F   T   F   F   F   T   F   F   F   T   F   F   F   T   F   F
//  y3  -   -   -   F   F   T   F   F   F   T   F   F   F   T   F   F   F   T   F   F   F   T   F
//  y4  -   -   -   F   F   F   T   F   F   F   T   F   F   F   T   F   F   F   T   F   F   F   T
//  --------------------------------------------------------------------------------------------
//  a1  X   X   X   -   -   -   -   -   -   -   -   X   -   -   X   X   X   X   X   X   X   X   X
//  a2  -   -   -   X   X   X   X   -   X   X   -   -   -   -   -   -   -   -   -   -   -   -   -
//  a3  -   -   -   -   -   -   -   X   -   -   X   -   X   X   -   -   -   -   -   -   -   -   -
//  a4  -   -   -   -   -   -   -   X   -   -   X   -   X   X   -   -   -   -   -   -   -   -   -
//  a5  -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -
//  a6  -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -
  ASSERT_EQ("Not a Date", nextDate(6, 4, 2013));
  ASSERT_EQ("Not a Date", nextDate(7, 13, 1900));
  ASSERT_EQ("Not a Date", nextDate(0, 5, 2000));
  // m == 2 // d1
  ASSERT_EQ("1877-2-6", nextDate(5, 2, 1877));
  ASSERT_EQ("1880-2-11", nextDate(10, 2, 1880));
  ASSERT_EQ("2000-2-16", nextDate(15, 2, 2000));
  ASSERT_EQ("1900-2-28", nextDate(27, 2, 1900));
  // d2
  ASSERT_EQ("1881-3-1", nextDate(28, 2, 1881));
  ASSERT_EQ("1884-2-29", nextDate(28, 2, 1884));
  ASSERT_EQ("2000-2-29", nextDate(28, 2, 2000));
  ASSERT_EQ("1900-3-1", nextDate(28, 2, 1900));
  // d3
  ASSERT_EQ("Not a Date", nextDate(29, 2, 1885));
  ASSERT_EQ("1888-3-1", nextDate(29, 2, 1888));
  ASSERT_EQ("2000-3-1", nextDate(29, 2, 2000));
  ASSERT_EQ("Not a Date", nextDate(29, 2, 1900));
  // d4
  ASSERT_EQ("Not a Date", nextDate(30, 2, 1889));
  ASSERT_EQ("Not a Date", nextDate(30, 2, 1892));
  ASSERT_EQ("Not a Date", nextDate(30, 2, 2000));
  ASSERT_EQ("Not a Date", nextDate(30, 2, 1900));
  // d5
  ASSERT_EQ("Not a Date", nextDate(31, 2, 1893));
  ASSERT_EQ("Not a Date", nextDate(31, 2, 1896));
  ASSERT_EQ("Not a Date", nextDate(31, 2, 2000));
  ASSERT_EQ("Not a Date", nextDate(31, 2, 1900));
//  ============================================================================================
//  c1  T   T   T   T   T   T   T   T   T   T   T   T   T   T   T   T   T   T   T   T
//  c2  T   T   T   T   T   T   T   T   T   T   T   T   T   T   T   T   T   T   T   T
//  c3  T   T   T   T   T   T   T   T   T   T   T   T   T   T   T   T   T   T   T   T
//  d1  T   T   T   T   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -
//  d2  -   -   -   -   T   T   T   T   -   -   -   -   -   -   -   -   -   -   -   -
//  d3  -   -   -   -   -   -   -   -   T   T   T   T   -   -   -   -   -   -   -   -
//  d4  -   -   -   -   -   -   -   -   -   -   -   -   T   T   T   T   -   -   -   -
//  d5  -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   T   T   T   T
//  m1  -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -
//  m2  T   T   T   T   T   T   T   T   T   T   T   T   T   T   T   T   T   T   T   T
//  m3  -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -
//  m4  -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -
//  y1  T   F   F   F   T   F   F   F   T   F   F   F   T   F   F   F   T   F   F   F
//  y2  F   T   F   F   F   T   F   F   F   T   F   F   F   T   F   F   F   T   F   F
//  y3  F   F   T   F   F   F   T   F   F   F   T   F   F   F   T   F   F   F   T   F
//  y4  F   F   F   T   F   F   F   T   F   F   F   T   F   F   F   T   F   F   F   T
//  --------------------------------------------------------------------------------
//  a1  -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   X   X   X   X
//  a2  X   X   X   X   X   X   X   X   X   X   X   X   -   -   -   -   -   -   -   -
//  a3  -   -   -   -   -   -   -   -   -   -   -   -   X   X   X   X   -   -   -   -
//  a4  -   -   -   -   -   -   -   -   -   -   -   -   X   X   X   X   -   -   -   -
//  a5  -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -
//  a6  -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -
  // m has 30 days // d1
  ASSERT_EQ("1901-4-6", nextDate(5, 4, 1901));
  ASSERT_EQ("1904-6-11", nextDate(10, 6, 1904));
  ASSERT_EQ("2000-9-16", nextDate(15, 9, 2000));
  ASSERT_EQ("1900-11-28", nextDate(27, 11, 1900));
  // d2
  ASSERT_EQ("1905-4-29", nextDate(28, 4, 1905));
  ASSERT_EQ("1908-6-29", nextDate(28, 6, 1908));
  ASSERT_EQ("2000-9-29", nextDate(28, 9, 2000));
  ASSERT_EQ("1900-11-29", nextDate(28, 11, 1900));
  // d3
  ASSERT_EQ("1909-4-30", nextDate(29, 4, 1909));
  ASSERT_EQ("1912-6-30", nextDate(29, 6, 1912));
  ASSERT_EQ("2000-9-30", nextDate(29, 9, 2000));
  ASSERT_EQ("1900-11-30", nextDate(29, 11, 1900));
  // d4
  ASSERT_EQ("1913-5-1", nextDate(30, 4, 1913));
  ASSERT_EQ("1916-7-1", nextDate(30, 6, 1916));
  ASSERT_EQ("2000-10-1", nextDate(30, 9, 2000));
  ASSERT_EQ("1900-12-1", nextDate(30, 11, 1900));
  // d5
  ASSERT_EQ("Not a Date", nextDate(31, 4, 1917));
  ASSERT_EQ("Not a Date", nextDate(31, 6, 1920));
  ASSERT_EQ("Not a Date", nextDate(31, 9, 2000));
  ASSERT_EQ("Not a Date", nextDate(31, 11, 1900));
//  ============================================================================================
//  c1  T   T   T   T   T   T   T   T   T   T   T   T   T   T   T   T   T   T   T   T
//  c2  T   T   T   T   T   T   T   T   T   T   T   T   T   T   T   T   T   T   T   T
//  c3  T   T   T   T   T   T   T   T   T   T   T   T   T   T   T   T   T   T   T   T
//  d1  T   T   T   T   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -
//  d2  -   -   -   -   T   T   T   T   -   -   -   -   -   -   -   -   -   -   -   -
//  d3  -   -   -   -   -   -   -   -   T   T   T   T   -   -   -   -   -   -   -   -
//  d4  -   -   -   -   -   -   -   -   -   -   -   -   T   T   T   T   -   -   -   -
//  d5  -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   T   T   T   T
//  m1  -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -
//  m2  -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -
//  m3  T   T   T   T   T   T   T   T   T   T   T   T   T   T   T   T   T   T   T   T
//  m4  -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -
//  y1  T   F   F   F   T   F   F   F   T   F   F   F   T   F   F   F   T   F   F   F
//  y2  F   T   F   F   F   T   F   F   F   T   F   F   F   T   F   F   F   T   F   F
//  y3  F   F   T   F   F   F   T   F   F   F   T   F   F   F   T   F   F   F   T   F
//  y4  F   F   F   T   F   F   F   T   F   F   F   T   F   F   F   T   F   F   F   T
//  --------------------------------------------------------------------------------
//  a1  -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -
//  a2  X   X   X   X   X   X   X   X   X   X   X   X   X   X   X   X   -   -   -   -
//  a3  -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   X   X   X   X
//  a4  -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   X   X   X   X
//  a5  -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -
//  a6  -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -
  // m has 31 days // d1
  ASSERT_EQ("1921-1-6", nextDate(5, 1, 1921));
  ASSERT_EQ("1924-3-11", nextDate(10, 3, 1924));
  ASSERT_EQ("2000-5-16", nextDate(15, 5, 2000));
  ASSERT_EQ("1900-7-28", nextDate(27, 7, 1900));
  // d2
  ASSERT_EQ("1925-8-29", nextDate(28, 8, 1925));
  ASSERT_EQ("1928-10-29", nextDate(28, 10, 1928));
  ASSERT_EQ("2000-1-29", nextDate(28, 1, 2000));
  ASSERT_EQ("1900-3-29", nextDate(28, 3, 1900));
  // d3
  ASSERT_EQ("1929-5-30", nextDate(29, 5, 1929));
  ASSERT_EQ("1932-7-30", nextDate(29, 7, 1932));
  ASSERT_EQ("2000-8-30", nextDate(29, 8, 2000));
  ASSERT_EQ("1900-10-30", nextDate(29, 10, 1900));
  // d4
  ASSERT_EQ("1933-1-31", nextDate(30, 1, 1933));
  ASSERT_EQ("1936-3-31", nextDate(30, 3, 1936));
  ASSERT_EQ("2000-5-31", nextDate(30, 5, 2000));
  ASSERT_EQ("1900-7-31", nextDate(30, 7, 1900));
  // d5
  ASSERT_EQ("1937-9-1", nextDate(31, 8, 1937));
  ASSERT_EQ("1940-11-1", nextDate(31, 10, 1940));
  ASSERT_EQ("2000-2-1", nextDate(31, 1, 2000));
  ASSERT_EQ("1900-4-1", nextDate(31, 3, 1900));
//  ============================================================================================
//  c1  T   T   T   T   T   T   T   T   T   T   T   T   T   T   T   T   T   T   T   T
//  c2  T   T   T   T   T   T   T   T   T   T   T   T   T   T   T   T   T   T   T   T
//  c3  T   T   T   T   T   T   T   T   T   T   T   T   T   T   T   T   T   T   T   T
//  d1  T   T   T   T   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -
//  d2  -   -   -   -   T   T   T   T   -   -   -   -   -   -   -   -   -   -   -   -
//  d3  -   -   -   -   -   -   -   -   T   T   T   T   -   -   -   -   -   -   -   -
//  d4  -   -   -   -   -   -   -   -   -   -   -   -   T   T   T   T   -   -   -   -
//  d5  -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   T   T   T   T
//  m1  -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -
//  m2  -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -
//  m3  -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -
//  m4  T   T   T   T   T   T   T   T   T   T   T   T   T   T   T   T   T   T   T   T
//  y1  T   F   F   F   T   F   F   F   T   F   F   F   T   F   F   F   T   F   F   F
//  y2  F   T   F   F   F   T   F   F   F   T   F   F   F   T   F   F   F   T   F   F
//  y3  F   F   T   F   F   F   T   F   F   F   T   F   F   F   T   F   F   F   T   F
//  y4  F   F   F   T   F   F   F   T   F   F   F   T   F   F   F   T   F   F   F   T
//  --------------------------------------------------------------------------------
//  a1  -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -
//  a2  X   X   X   X   X   X   X   X   X   X   X   X   X   X   X   X   -   -   -   -
//  a3  -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   X   X   X   X
//  a4  -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -
//  a5  -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   X   X   X   X
//  a6  -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   X   X   X   X
  // m == 12 // d1
  ASSERT_EQ("1941-12-6", nextDate(5, 12, 1941));
  ASSERT_EQ("1944-12-11", nextDate(10, 12, 1944));
  ASSERT_EQ("2000-12-16", nextDate(15, 12, 2000));
  ASSERT_EQ("1900-12-28", nextDate(27, 12, 1900));
  // d2
  ASSERT_EQ("1945-12-29", nextDate(28, 12, 1945));
  ASSERT_EQ("1948-12-29", nextDate(28, 12, 1948));
  ASSERT_EQ("2000-12-29", nextDate(28, 12, 2000));
  ASSERT_EQ("1900-12-29", nextDate(28, 12, 1900));
  // d3
  ASSERT_EQ("1949-12-30", nextDate(29, 12, 1949));
  ASSERT_EQ("1952-12-30", nextDate(29, 12, 1952));
  ASSERT_EQ("2000-12-30", nextDate(29, 12, 2000));
  ASSERT_EQ("1900-12-30", nextDate(29, 12, 1900));
  // d4
  ASSERT_EQ("1953-12-31", nextDate(30, 12, 1953));
  ASSERT_EQ("1956-12-31", nextDate(30, 12, 1956));
  ASSERT_EQ("2000-12-31", nextDate(30, 12, 2000));
  ASSERT_EQ("1900-12-31", nextDate(30, 12, 1900));
  // d5
  ASSERT_EQ("1958-1-1", nextDate(31, 12, 1957));
  ASSERT_EQ("1961-1-1", nextDate(31, 12, 1960));
  ASSERT_EQ("2001-1-1", nextDate(31, 12, 2000));
  ASSERT_EQ("1901-1-1", nextDate(31, 12, 1900));
}

//  d1:   1 <= d <= 27        any day
//  d2:   d == 28
//  d3:   d == 29
//  d4:   d == 30
//  d5:   d == 31
//  m1:   m == 2
//  m2:   month has 30 days
//  m3:   month has 31 days
//  m4:   m == 12
//  y1:   y is any year
//  y2:   y is a leap year

//  a1: Not a Date
//  a2: increment day
//  a3: reset day
//  a4: increment month
//  a5: reset month
//  a6: increment year
TEST(NextDateTests, Reduced_Decision_Table){
//      m1    m1    m1    m1    m1    m1    m1
//      d1    d2    d2    d3    d3    d4    d5
//      -     y1    y2    y1    y2    -     -
//  -----------------------------------------
//  a1  -     -     -     X     -     X     X
//  a2  X     -     X     -     -     -     -
//  a3  -     X     -     -     X     -     -
//  a4  -     X     -     -     X     -     -
//  a5  -     -     -     -     -     -     -
//  a6  -     -     -     -     -     -     -
  ASSERT_EQ("1819-2-16", nextDate(15, 2, 1819));
  ASSERT_EQ("2001-3-1", nextDate(28, 2, 2001));
  ASSERT_EQ("2000-2-29", nextDate(28, 2, 2000));
  ASSERT_EQ("Not a Date", nextDate(29, 2, 1915));
  ASSERT_EQ("1944-3-1", nextDate(29, 2, 1944));
  ASSERT_EQ("Not a Date", nextDate(30, 2, 1854));
  ASSERT_EQ("Not a Date", nextDate(31, 2, 1855));
//  =========================================
//      m2        m2    m2
//      d1,d2,d3  d4    d5
//      -         -     -
//  -----------------------------------------
//  a1  -         -     X
//  a2  X         -     -
//  a3  -         X     -
//  a4  -         X     -
//  a5  -         -     -
//  a6  -         -     -
  ASSERT_EQ("2005-4-30", nextDate(29, 4, 2005));
  ASSERT_EQ("1912-7-1", nextDate(30, 6, 1912));
  ASSERT_EQ("Not a Date", nextDate(31, 9, 2000));
//  =========================================
//      m3            m3
//      d1,d2,d3,d4   d5
//      -             -
//  -----------------------------------------
//  a1  -             -
//  a2  X             -
//  a3  -             X
//  a4  -             X
//  a5  -             -
//  a6  -             -
  ASSERT_EQ("1866-10-17", nextDate(16, 10, 1866));
  ASSERT_EQ("1978-9-1", nextDate(31, 8, 1978));
//  =========================================
//      m4            m4
//      d1,d2,d3,d4   d5
//      -             -
//  -----------------------------------------
//  a1  -             -
//  a2  X             -
//  a3  -             X
//  a4  -             -
//  a5  -             X
//  a6  -             X
  ASSERT_EQ("1912-12-26", nextDate(25, 12, 1912));
  ASSERT_EQ("1993-1-1", nextDate(31, 12, 1992));
}

