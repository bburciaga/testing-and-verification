# Lab 4: Next Date Function

The purpose of this repository is to be able
to build, run, and test the nextDate funciton.
As well to understand how Desision Table-Based
testing works, and how to use doxygen to help
document my code.

We also look into hosting doxygen onto a
website using the gitlab-ci.yaml files.
> Did not understand and got stuck on this
> part.

### Doxygen

Software that is used to generate documentation 
for multiple languages. I'll be using it for
C++.

### Next Date Function

The function will have three inputs:
  - month `m`
  - day `d`
  - year `y`

It will return the next date of the inputed
date.
The bounds are:
  - c1. `1 <= m <= 12`
  - c2. `1 <= d <= 31`
  - c3. `1812 <= y <= 2012`

### Testing Methods

- Decision Table
- Extended Entry Dection Table
- Reduced Decision Table

