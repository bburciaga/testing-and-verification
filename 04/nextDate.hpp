/**
 * @file nextDate.hpp
 *
 * @brief header file to call the nextDate function in other directories
 *
 * @author Brian Burciaga T00566000
 * Contact: burciagab16@mytru.ca
 * 
 */
#include <iostream>
#include <string>

#ifndef NEXTDATE_HPP
#define NEXTDATE_HPP

std::string nextDate(int m, int d, int y);

#endif

