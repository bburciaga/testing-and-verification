/**
 * @file nextDate.cpp
 *
 * @brief Contains function to get the next date with helper function
 *
 * @author Brian Burciaga T00566000
 * Contact: burciagab16@mytru.ca
 *
 */
#include <iostream>
#include <string>
#include "nextDate.hpp"

/**
 * isLeapYear
 * @description function to determine if the value is a leap year
 * @param y specified year
 * @return true or false if the value is a leap year
 */
int isLeapYear(int y) {
  if (y % 4 == 0) {
    if (y % 100 == 0) {
      if (y % 400 == 0) {
        return 1;
      }
      return 0;
    }
    return 1;
  }
  return 0;
}
/**
 * isValidDate
 * @description check to see if the inputed date is valid in regards to the months
 * @param d day of the month
 * @param m month of the year
 * @param isLeapYear used to check February in case of leap year
 * @return true or false if the date is valid 
 */
int isValidDate(int d, int m, int isLeapYear) {
    switch (m) {
      case 1:
      case 3:
      case 5:
      case 7:
      case 8:
      case 10:
      case 12:
        return 1;
        break;
      case 2: 
        if (isLeapYear && d < 30) return 1;
        if (d < 29) return 1;
        break;
      default:
        if (d < 31) return 1; 
    }
  return 0;
}

/**
 * incrementNum
 * @description increments the inputed number
 * @param n integer value
 * @return n + 1
 */
int incrementNum (int n) {
  return n+1;
}

/**
 * resetNum
 * @description used to reset a value to 1
 * @return 1
 */
int resetNum () {
  return 1;
}

/**
 * incrementDate
 * @description used to increment an inputed date
 * @param d day of the month
 * @param m month of the year
 * @param y any year
 * @return string
 */
std::string incrementDate(int d, int m, int y) {
  int newD = incrementNum(d);
  int newM = m;
  int newY = y;
  int leapYear = isLeapYear(y);

  switch (m) {
    case 1:
    case 3:
    case 5:
    case 7:
    case 8:
    case 10:
    case 12:
      if (newD > 31) {
        newD = resetNum();
        newM = incrementNum(m);
      }
      break;
    case 2:
      if (leapYear && newD > 29) {
        newD = resetNum();
        newM = incrementNum(m);
      }
      if (!leapYear && newD > 28) {
        newD = resetNum();
        newM = incrementNum(m);
      }
      break;
    default:
      if (newD > 30) {
        newD = resetNum();
        newM = incrementNum(m);
      }
  }

  if (newM > 12) {
    newM = resetNum();
    newY = incrementNum(y);
  }

  return std::to_string(newY) + "-" + std::to_string(newM) + "-" + std::to_string(newD);
}

/**
 * nextDate
 * @description input a date to get the next date
 * @param d day of the month
 * @param m month between 1 to 12
 * @param y any year between 1812-2012
 * @return either "Not a Date" or the next date
 */
std::string nextDate(int d, int m, int y) {
  // Checking if it's a valid input
  if (m >= 1 && m <= 12 && d >= 1 && d <= 31 && y >= 1812 && y <= 2012) {
    if (isValidDate(d, m, isLeapYear(y))) {
      return incrementDate(d, m, y);
    }
  }
  return std::string("Not a Date");
}

