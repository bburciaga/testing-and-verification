/**
 * @file tests.cpp
 *
 * @brief ECT tests to run the nextDate function to ensure it is working
 *
 * @author Brian Burciaga T00566000
 * Contact: burciagab16@mytru.ca
 * 
 */
#include <iostream>
#include <string>
#include <gtest/gtest.h>
#include "nextDate.hpp"

// Y1 = {year: y % 4 == 0}
// Y2 = {year: y % 100 == 0 && y % 400 == 0}
// Y3 = {year: 1812, ..., 2012 }
// M1 = {month: m == 2}
// M2 = {month: has 30 days}
// M3 = {month: has 31 days}
// D1 = {day: 1, ..., 28}
// D2 = {day: 1, ..., 29}
// D3 = {day: 1, ..., 30}
// D4 = {day: 1, ..., 31}

TEST(NextDateTests, Traditional_ECT) {
  // Case 1     D       M       Y
  ASSERT_EQ("1990-7-24", nextDate(23, 7, 1990));
  // Case 2     D-      M       Y
  ASSERT_EQ("Not a Date", nextDate(0, 2, 1942));
  // Case 3     D+      M       Y
  ASSERT_EQ("Not a Date", nextDate(32, 12, 1960));
  // Case 4     D       M-      Y
  ASSERT_EQ("Not a Date", nextDate(7, 0, 1912));
  // Case 5     D       M+      Y
  ASSERT_EQ("Not a Date", nextDate(17, 13, 2010));
  // Case 6     D       M       Y-
  ASSERT_EQ("Not a Date", nextDate(28, 2, 1811));
  // Case 7     D       M       Y+
  ASSERT_EQ("Not a Date", nextDate(3, 3, 2013));
}

TEST(NextDateTests, Weak_ECT) {
  // Case 1     D+/-    M+-     Y1  
  ASSERT_EQ("Not a Date", nextDate(30, 2, 1912));
  // Case 2     D+/-    M+-     Y2  
  ASSERT_EQ("Not a Date", nextDate(0, 7, 2000));
  // Case 3     D+/-    M+-     Y3  
  ASSERT_EQ("Not a Date", nextDate(13, 13, 1973));
  // Case 4     D+/-    M+-     Y1  
  ASSERT_EQ("1952-3-1", nextDate(29, 2, 1952));
  // Case 5     D+/-    M+-     Y2  
  ASSERT_EQ("2000-2-29", nextDate(28, 2, 2000));
  // Case 6     D+/-    M+-     Y3  
  ASSERT_EQ("1934-1-1", nextDate(31, 12, 1933));
}

TEST(NextDateTests, Strong_ECT) {
  // Case 1     D1      M1      Y1
  ASSERT_EQ("2008-2-29", nextDate(28, 2, 2008));
  // Case 2     D1      M1      Y2
  ASSERT_EQ("2000-2-2", nextDate(1, 2, 2000));
  // Case 3     D1      M1      Y3
  ASSERT_EQ("1813-3-1", nextDate(28, 2, 1813));
  // Case 4     D1      M2      Y1
  ASSERT_EQ("1992-4-11", nextDate(10, 4, 1992));
  // Case 5     D1      M2      Y2
  ASSERT_EQ("2000-6-16", nextDate(15, 6, 2000));
  // Case 6     D1      M2      Y3
  ASSERT_EQ("1814-9-21", nextDate(20, 9, 1814));
  // Case 7     D1      M3      Y1
  ASSERT_EQ("1816-1-26", nextDate(25, 1, 1816));
  // Case 8     D1      M3      Y2
  ASSERT_EQ("2000-3-3", nextDate(2, 3, 2000));
  // Case 9     D1      M3      Y3
  ASSERT_EQ("1815-5-7", nextDate(6, 5, 1815));
  //  -------------------------
  // Case 10     D2      M1      Y1
  ASSERT_EQ("1956-2-9", nextDate(8, 2, 1956));
  // Case 11     D2      M1      Y2
  ASSERT_EQ("2000-3-1", nextDate(29, 2, 2000));
  // Case 12     D2      M1      Y3
  ASSERT_EQ("1818-2-11", nextDate(10, 2, 1818));
  // Case 13     D2      M2      Y1
  ASSERT_EQ("1960-11-23", nextDate(22, 11, 1960));
  // Case 14     D2      M2      Y2
  ASSERT_EQ("2000-4-27", nextDate(26, 4, 2000));
  // Case 15     D2      M2      Y3
  ASSERT_EQ("1819-6-17", nextDate(16, 6, 1819));
  // Case 16     D2      M3      Y1
  ASSERT_EQ("1984-7-4", nextDate(3, 7, 1984));
  // Case 17     D2      M3      Y2
  ASSERT_EQ("2000-8-8", nextDate(7, 8, 2000));
  // Case 18     D2      M3      Y3
  ASSERT_EQ("1821-10-14", nextDate(13, 10, 1821));
  //  -------------------------
  // Case 19     D3      M1      Y1
  ASSERT_EQ("2008-2-18", nextDate(17, 2, 2008));
  // Case 20     D3      M1      Y2
  ASSERT_EQ("2000-2-24", nextDate(23, 2, 2000));
  // Case 21     D3      M1      Y3
  ASSERT_EQ("1823-2-28", nextDate(27, 2, 1823));
  // Case 22     D3      M2      Y1
  ASSERT_EQ("1972-10-1", nextDate(30, 9, 1972));
  // Case 23     D3      M2      Y2
  ASSERT_EQ("2000-11-5", nextDate(4, 11, 2000));
  // Case 24     D3      M2      Y3
  ASSERT_EQ("1825-4-10", nextDate(9, 4, 1825));
  // Case 25     D3      M3      Y1
  ASSERT_EQ("1992-12-15", nextDate(14, 12, 1992));
  // Case 26     D3      M3      Y2
  ASSERT_EQ("2000-1-20", nextDate(19, 1, 2000));
  // Case 27     D3      M3      Y3
  ASSERT_EQ("1826-3-25", nextDate(24, 3, 1826));
  //  -------------------------
  // Case 28     D4      M1      Y1
  ASSERT_EQ("1892-3-1", nextDate(29, 2, 1892));
  // Case 29     D4      M1      Y2
  ASSERT_EQ("2000-2-2", nextDate(1, 2, 2000));
  // Case 30     D4      M1      Y3
  ASSERT_EQ("1829-2-6", nextDate(5, 2, 1829));
  // Case 31     D4      M2      Y1
  ASSERT_EQ("1820-9-11", nextDate(10, 9, 1820));
  // Case 32     D4      M2      Y2
  ASSERT_EQ("2000-11-16", nextDate(15, 11, 2000));
  // Case 33     D4      M2      Y3
  ASSERT_EQ("1830-4-21", nextDate(20, 4, 1830));
  // Case 34     D4      M3      Y1
  ASSERT_EQ("1872-5-26", nextDate(25, 5, 1872));
  // Case 35     D4      M3      Y2
  ASSERT_EQ("2000-8-1", nextDate(31, 7, 2000));
  // Case 36     D4      M3      Y3
  ASSERT_EQ("1831-9-1", nextDate(31, 8, 1831));
}

TEST(NextDateTests, Weak_Robust_ECT) {
  // Case 1     D-      M       Y
  ASSERT_EQ("Not a Date", nextDate(0, 5, 1991));
  // Case 2     D+      M       Y
  ASSERT_EQ("Not a Date", nextDate(32, 6, 1992));
  // Case 3     D       M-      Y
  ASSERT_EQ("Not a Date", nextDate(5, 0, 1993));
  // Case 4     D       M+      Y
  ASSERT_EQ("Not a Date", nextDate(6, 13, 1994));
  // Case 5     D       M       Y-
  ASSERT_EQ("Not a Date", nextDate(7, 7,1811));
  // Case 6     D       M       Y+
  ASSERT_EQ("Not a Date", nextDate(8, 8, 2013));
  // Case 2     D       M       Y1     INVALID
  ASSERT_EQ("Not a Date", nextDate(30, 2, 1972));
  // Case 3     D       M       Y2     INVALID
  ASSERT_EQ("Not a Date", nextDate(10, 13, 2000));
  // Case 4     D       M       Y3     INVALID
  ASSERT_EQ("Not a Date", nextDate(31, 11, 1973));
  // Case 5     D       M       Y1     VALID
  ASSERT_EQ("1972-12-13", nextDate(12, 12, 1972));
  // Case 6     D       M       Y2     VALID
  ASSERT_EQ("2000-1-14", nextDate(13, 1, 2000));
  // Case 7     D       M       Y3     VALID
  ASSERT_EQ("1973-2-15", nextDate(14, 2, 1973));
}

TEST(NextDateTests, Weak_Robust_Rev_ECT) {}

TEST(NextDateTests, Strong_Robust_ECT) {
  //  Case 1      D-      M-      Y-
  ASSERT_EQ("Not a Date", nextDate(0, 0, 1811));
  //  Case 2      D-      M-      Y1
  ASSERT_EQ("Not a Date", nextDate(0, 0, 1812));
  //  Case 3      D-      M-      Y2
  ASSERT_EQ("Not a Date", nextDate(0, 0, 2000));
  //  Case 4      D-      M-      Y3
  ASSERT_EQ("Not a Date", nextDate(0, 0, 1901));
  //  Case 5      D-      M-      Y+
  ASSERT_EQ("Not a Date", nextDate(0, 0, 2013));
  //  ---------------------------
  //  Case 6      D-      M1      Y-
  ASSERT_EQ("Not a Date", nextDate(0, 2, 1811));
  //  Case 7      D-      M1      Y1
  ASSERT_EQ("Not a Date", nextDate(0, 2, 1816));
  //  Case 8      D-      M1      Y2
  ASSERT_EQ("Not a Date", nextDate(0, 2, 2000));
  //  Case 9      D-      M1      Y3
  ASSERT_EQ("Not a Date", nextDate(0, 2, 1903));
  //  Case 10      D-      M1      Y+
  ASSERT_EQ("Not a Date", nextDate(0, 2, 2013));
  //  ---------------------------
  //  Case 11      D-      M2      Y-
  ASSERT_EQ("Not a Date", nextDate(0, 4, 1811));
  //  Case 12      D-      M2      Y1
  ASSERT_EQ("Not a Date", nextDate(0, 6, 1820));
  //  Case 13      D-      M2      Y2
  ASSERT_EQ("Not a Date", nextDate(0, 9, 2000));
  //  Case 14      D-      M2      Y3
  ASSERT_EQ("Not a Date", nextDate(0, 11, 1905));
  //  Case 15      D-      M2      Y+
  ASSERT_EQ("Not a Date", nextDate(0, 4, 2013));
  //  ---------------------------
  //  Case 16      D-      M3      Y-
  ASSERT_EQ("Not a Date", nextDate(0, 1, 1811));
  //  Case 17      D-      M3      Y1
  ASSERT_EQ("Not a Date", nextDate(0, 3, 1824));
  //  Case 18      D-      M3      Y2
  ASSERT_EQ("Not a Date", nextDate(0, 5, 2000));
  //  Case 19      D-      M3      Y3
  ASSERT_EQ("Not a Date", nextDate(0, 7, 1907));
  //  Case 20      D-      M3      Y+
  ASSERT_EQ("Not a Date", nextDate(0, 8, 2013));
  //  ---------------------------
  //  Case 21      D-      M+      Y-
  ASSERT_EQ("Not a Date", nextDate(0, 13, 1811));
  //  Case 22      D-      M+      Y1
  ASSERT_EQ("Not a Date", nextDate(0, 13, 1828));
  //  Case 23      D-      M+      Y2
  ASSERT_EQ("Not a Date", nextDate(0, 13, 2000));
  //  Case 24      D-      M+      Y3
  ASSERT_EQ("Not a Date", nextDate(0, 13, 1910));
  //  Case 25      D-      M+      Y+
  ASSERT_EQ("Not a Date", nextDate(0, 13, 2013));
  //  ===========================
  //  Case 26      D1      M-      Y-
  ASSERT_EQ("Not a Date", nextDate(28, 0, 1811));
  //  Case 27      D1      M-      Y1
  ASSERT_EQ("Not a Date", nextDate(27, 0, 1832));
  //  Case 28      D1      M-      Y2
  ASSERT_EQ("Not a Date", nextDate(26, 0, 2000));
  //  Case 29      D1      M-      Y3
  ASSERT_EQ("Not a Date", nextDate(25, 0, 1913));
  //  Case 30      D1      M-      Y+
  ASSERT_EQ("Not a Date", nextDate(24, 0, 2013));
  //  ---------------------------
  //  Case 31      D1      M1      Y-
  ASSERT_EQ("Not a Date", nextDate(23, 2, 1811));
  //  Case 32      D1      M1      Y1
  ASSERT_EQ("1836-2-29", nextDate(28, 2, 1836));
  //  Case 33      D1      M1      Y2
  ASSERT_EQ("2000-2-29", nextDate(28, 2, 2000));
  //  Case 34      D1      M1      Y3
  ASSERT_EQ("1905-3-1", nextDate(28, 2, 1905));
  //  Case 35      D1      M1      Y+
  ASSERT_EQ("Not a Date", nextDate(14, 2, 2013));
  //  ---------------------------
  //  Case 36      D1      M2      Y-
  ASSERT_EQ("Not a Date", nextDate(10, 4, 1811));
  //  Case 37      D1      M2      Y1
  ASSERT_EQ("1840-6-21", nextDate(20, 6, 1840));
  //  Case 38      D1      M2      Y2
  ASSERT_EQ("2000-9-29", nextDate(28, 9, 2000));
  //  Case 39      D1      M2      Y3
  ASSERT_EQ("1914-11-6", nextDate(5, 11, 1914));
  //  Case 40      D1      M2      Y+
  ASSERT_EQ("Not a Date", nextDate(11, 4, 2013));
  //  ---------------------------
  //  Case 41      D1      M3      Y-
  ASSERT_EQ("Not a Date", nextDate(7, 10, 1811));
  //  Case 42      D1      M3      Y1
  ASSERT_EQ("1844-12-29", nextDate(28, 12, 1844));
  //  Case 43      D1      M3      Y2
  ASSERT_EQ("2000-1-9", nextDate(8, 1, 2000));
  //  Case 44      D1      M3      Y3
  ASSERT_EQ("1917-3-12", nextDate(11, 3, 1917));
  //  Case 45      D1      M3      Y+
  ASSERT_EQ("Not a Date", nextDate(9, 5, 2013));
  //  ---------------------------
  //  Case 46      D1      M+      Y-
  ASSERT_EQ("Not a Date", nextDate(5, 13, 1811));
  //  Case 47      D1      M+      Y1
  ASSERT_EQ("Not a Date", nextDate(3, 13, 1848));
  //  Case 48      D1      M+      Y2
  ASSERT_EQ("Not a Date", nextDate(1, 13, 2000));
  //  Case 49      D1      M+      Y3
  ASSERT_EQ("Not a Date", nextDate(13, 13, 1918));
  //  Case 50      D1      M+      Y+
  ASSERT_EQ("Not a Date", nextDate(15, 13, 2013));
  //  ===========================
  //  Case 51      D2      M-      Y-
  ASSERT_EQ("Not a Date", nextDate(24, 0, 1811));
  //  Case 52      D2      M-      Y1
  ASSERT_EQ("Not a Date", nextDate(7, 0, 1852));
  //  Case 53      D2      M-      Y2
  ASSERT_EQ("Not a Date", nextDate(4, 0, 2000));
  //  Case 54      D2      M-      Y3
  ASSERT_EQ("Not a Date", nextDate(29, 0, 1921));
  //  Case 55      D2      M-      Y+
  ASSERT_EQ("Not a Date", nextDate(23, 0, 2013));
  //  ---------------------------
  //  Case 56      D2      M1      Y-
  ASSERT_EQ("Not a Date", nextDate(2, 2, 1811));
  //  Case 57      D2      M1      Y1
  ASSERT_EQ("1856-3-1", nextDate(29, 2, 1856));
  //  Case 58      D2      M1      Y2
  ASSERT_EQ("2000-2-29", nextDate(28, 2, 2000));
  //  Case 59      D2      M1      Y3
  ASSERT_EQ("1922-2-5", nextDate(4, 2, 1922));
  //  Case 60      D2      M1      Y+
  ASSERT_EQ("Not a Date", nextDate(23, 2, 2013));
  //  ---------------------------
  //  Case 61      D2      M2      Y-
  ASSERT_EQ("Not a Date", nextDate(23, 4, 1811));
  //  Case 62      D2      M2      Y1
  ASSERT_EQ("1860-6-10", nextDate(9, 6, 1860));
  //  Case 63      D2      M2      Y2
  ASSERT_EQ("2000-9-20", nextDate(19, 9, 2000));
  //  Case 64      D2      M2      Y3
  ASSERT_EQ("1925-11-9", nextDate(8, 11, 1925));
  //  Case 65      D2      M2      Y+
  ASSERT_EQ("Not a Date", nextDate(4, 4, 2013));
  //  ---------------------------
  //  Case 66      D2      M3      Y-
  ASSERT_EQ("Not a Date", nextDate(12, 10, 1811));
  //  Case 67      D2      M3      Y1
  ASSERT_EQ("1864-12-30", nextDate(29, 12, 1864));
  //  Case 68      D2      M3      Y2
  ASSERT_EQ("2000-1-6", nextDate(5, 1, 2000));
  //  Case 69      D2      M3      Y3
  ASSERT_EQ("1926-3-12", nextDate(11, 3, 1926));
  //  Case 70      D2      M3      Y+
  ASSERT_EQ("Not a Date", nextDate(5, 5, 2013));
  //  ---------------------------
  //  Case 71      D2      M+      Y-
  ASSERT_EQ("Not a Date", nextDate(9, 13, 1811));
  //  Case 72      D2      M+      Y1
  ASSERT_EQ("Not a Date", nextDate(13, 13, 1868));
  //  Case 73      D2      M+      Y2
  ASSERT_EQ("Not a Date", nextDate(17, 13, 2000));
  //  Case 74      D2      M+      Y3
  ASSERT_EQ("Not a Date", nextDate(29, 13, 1929));
  //  Case 75      D2      M+      Y+
  ASSERT_EQ("Not a Date", nextDate(23, 13, 2013));
  //  ===========================
  //  Case 76      D3      M-      Y-
  ASSERT_EQ("Not a Date", nextDate(30, 0, 1811));
  //  Case 77      D3      M-      Y1
  ASSERT_EQ("Not a Date", nextDate(4, 0, 1872));
  //  Case 78      D3      M-      Y2
  ASSERT_EQ("Not a Date", nextDate(30, 0, 2000));
  //  Case 79      D3      M-      Y3
  ASSERT_EQ("Not a Date", nextDate(4, 0, 1931));
  //  Case 80      D3      M-      Y+
  ASSERT_EQ("Not a Date", nextDate(9, 0, 2013));
  //  ---------------------------
  //  Case 81      D3      M1      Y-
  ASSERT_EQ("Not a Date", nextDate(5, 2, 1811));
  //  Case 82      D3      M1      Y1
  ASSERT_EQ("1876-3-1", nextDate(29, 2, 1876));
  //  Case 83      D3      M1      Y2
  ASSERT_EQ("2000-2-14", nextDate(13, 2, 2000));
  //  Case 84      D3      M1      Y3
  ASSERT_EQ("1933-2-10", nextDate(9, 2, 1933));
  //  Case 85      D3      M1      Y+
  ASSERT_EQ("Not a Date", nextDate(19, 2, 2013));
  //  ---------------------------
  //  Case 86      D3      M2      Y-
  ASSERT_EQ("Not a Date", nextDate(14, 4, 1811));
  //  Case 87      D3      M2      Y1
  ASSERT_EQ("1880-7-1", nextDate(30, 6, 1880));
  //  Case 88      D3      M2      Y2
  ASSERT_EQ("2000-10-1", nextDate(30, 9, 2000));
  //  Case 89      D3      M2      Y3
  ASSERT_EQ("1934-11-5", nextDate(4, 11, 1934));
  //  Case 90      D3      M2      Y+
  ASSERT_EQ("Not a Date", nextDate(6, 4, 2013));
  //  ---------------------------
  //  Case 91      D3      M3      Y-
  ASSERT_EQ("Not a Date", nextDate(6, 10, 1811));
  //  Case 92      D3      M3      Y1
  ASSERT_EQ("1884-12-31", nextDate(30, 12, 1884));
  //  Case 93      D3      M3      Y2
  ASSERT_EQ("2000-1-3", nextDate(2, 1, 2000));
  //  Case 94      D3      M3      Y3
  ASSERT_EQ("1937-3-31", nextDate(30, 3, 1937));
  //  Case 95      D3      M3      Y+
  ASSERT_EQ("Not a Date", nextDate(5, 5, 2013));
  //  ---------------------------
  //  Case 96      D3      M+      Y-
  ASSERT_EQ("Not a Date", nextDate(6, 13, 1811));
  //  Case 97      D3      M+      Y1
  ASSERT_EQ("Not a Date", nextDate(9, 13, 1888));
  //  Case 98      D3      M+      Y2
  ASSERT_EQ("Not a Date", nextDate(4, 13, 2000));
  //  Case 99      D3      M+      Y3
  ASSERT_EQ("Not a Date", nextDate(2, 13, 1939));
  //  Case 100      D3      M+      Y+
  ASSERT_EQ("Not a Date", nextDate(0, 13, 2013));
  //  ===========================
  //  Case 101      D4      M-      Y-
  ASSERT_EQ("Not a Date", nextDate(8, 0, 1811));
  //  Case 102      D4      M-      Y1
  ASSERT_EQ("Not a Date", nextDate(0, 0, 1892));
  //  Case 103      D4      M-      Y2
  ASSERT_EQ("Not a Date", nextDate(0, 0, 2000));
  //  Case 104      D4      M-      Y3
  ASSERT_EQ("Not a Date", nextDate(8, 0, 1941));
  //  Case 105      D4      M-      Y+
  ASSERT_EQ("Not a Date", nextDate(5, 0, 2013));
  //  ---------------------------
  //  Case 106      D4      M1      Y-
  ASSERT_EQ("Not a Date", nextDate(6, 2, 1811));
  //  Case 107      D4      M1      Y1
  ASSERT_EQ("1896-2-8", nextDate(7, 2, 1896));
  //  Case 108      D4      M1      Y2
  ASSERT_EQ("2000-2-5", nextDate(4, 2, 2000));
  //  Case 109      D4      M1      Y3
  ASSERT_EQ("1943-2-9", nextDate(8, 2, 1943));
  //  Case 110      D4      M1      Y+
  ASSERT_EQ("Not a Date", nextDate(9, 2, 2013));
  //  ---------------------------
  //  Case 111      D4      M2      Y-
  ASSERT_EQ("Not a Date", nextDate(5, 4, 1811));
  //  Case 112      D4      M2      Y1
  ASSERT_EQ("1904-6-7", nextDate(6, 6, 1904));
  //  Case 113      D4      M2      Y2
  ASSERT_EQ("2000-9-8", nextDate(7, 9, 2000));
  //  Case 114      D4      M2      Y3
  ASSERT_EQ("1945-11-9", nextDate(8, 11, 1945));
  //  Case 115      D4      M2      Y+
  ASSERT_EQ("Not a Date", nextDate(9, 4, 2013));
  //  ---------------------------
  //  Case 116      D4      M3      Y-
  ASSERT_EQ("Not a Date", nextDate(9, 10, 1811));
  //  Case 117      D4      M3      Y1
  ASSERT_EQ("1909-1-1", nextDate(31, 12, 1908));
  //  Case 118      D4      M3      Y2
  ASSERT_EQ("2000-1-30", nextDate(29, 1, 2000));
  //  Case 119      D4      M3      Y3
  ASSERT_EQ("1947-3-4", nextDate(3, 3, 1947));
  //  Case 120      D4      M3      Y+
  ASSERT_EQ("Not a Date", nextDate(6, 5, 2013));
  //  ---------------------------
  //  Case 121      D4      M+      Y-
  ASSERT_EQ("Not a Date", nextDate(31, 13, 1811));
  //  Case 122      D4      M+      Y1
  ASSERT_EQ("Not a Date", nextDate(31, 13, 1912));
  //  Case 123      D4      M+      Y2
  ASSERT_EQ("Not a Date", nextDate(31, 13, 2000));
  //  Case 124      D4      M+      Y3
  ASSERT_EQ("Not a Date", nextDate(31, 13, 1949));
  //  Case 125      D4      M+      Y+
  ASSERT_EQ("Not a Date", nextDate(31, 13, 2013));
  //  ===========================
  //  Case 126      D+      M-      Y-
  ASSERT_EQ("Not a Date", nextDate(32, 0, 1811));
  //  Case 127      D+      M-      Y1
  ASSERT_EQ("Not a Date", nextDate(32, 0, 1916));
  //  Case 128      D+      M-      Y2
  ASSERT_EQ("Not a Date", nextDate(32, 0, 2000));
  //  Case 129      D+      M-      Y3
  ASSERT_EQ("Not a Date", nextDate(32, 0, 1950));
  //  Case 130      D+      M-      Y+
  ASSERT_EQ("Not a Date", nextDate(32, 0, 2013));
  //  ---------------------------
  //  Case 131      D+      M1      Y-
  ASSERT_EQ("Not a Date", nextDate(32, 2, 1811));
  //  Case 132      D+      M1      Y1
  ASSERT_EQ("Not a Date", nextDate(32, 2, 1920));
  //  Case 133      D+      M1      Y2
  ASSERT_EQ("Not a Date", nextDate(32, 2, 2000));
  //  Case 134      D+      M1      Y3
  ASSERT_EQ("Not a Date", nextDate(32, 2, 1953));
  //  Case 135      D+      M1      Y+
  ASSERT_EQ("Not a Date", nextDate(32, 2, 2013));
  //  ---------------------------
  //  Case 136      D+      M2      Y-
  ASSERT_EQ("Not a Date", nextDate(32, 4, 1811));
  //  Case 137      D+      M2      Y1
  ASSERT_EQ("Not a Date", nextDate(32, 6, 1924));
  //  Case 138      D+      M2      Y2
  ASSERT_EQ("Not a Date", nextDate(32, 9, 2000));
  //  Case 139      D+      M2      Y3
  ASSERT_EQ("Not a Date", nextDate(32, 11, 1954));
  //  Case 140      D+      M2      Y+
  ASSERT_EQ("Not a Date", nextDate(32, 4, 2013));
  //  ---------------------------
  //  Case 141      D+      M3      Y-
  ASSERT_EQ("Not a Date", nextDate(32, 1, 1811));
  //  Case 142      D+      M3      Y1
  ASSERT_EQ("Not a Date", nextDate(32, 3, 1928));
  //  Case 143      D+      M3      Y2
  ASSERT_EQ("Not a Date", nextDate(32, 5, 2000));
  //  Case 144      D+      M3      Y3
  ASSERT_EQ("Not a Date", nextDate(32, 7, 1957));
  //  Case 145      D+      M3      Y+
  ASSERT_EQ("Not a Date", nextDate(32, 8, 2013));
  //  ---------------------------
  //  Case 146      D+   32  M+      Y-
  ASSERT_EQ("Not a Date", nextDate(32, 13, 1811));
  //  Case 147      D+   32  M+      Y1
  ASSERT_EQ("Not a Date", nextDate(32, 13, 1932));
  //  Case 148      D+   32  M+      Y2
  ASSERT_EQ("Not a Date", nextDate(32, 13, 2000));
  //  Case 149      D+   32  M+      Y3
  ASSERT_EQ("Not a Date", nextDate(32, 13, 1977));
  //  Case 150      D+   32  M+      Y+
  ASSERT_EQ("Not a Date", nextDate(32, 13, 2013));
}

TEST(NextDateTests, Special_Value_Testing) {
  //  Case 1        D1max    M1       Y3
  ASSERT_EQ("2003-3-1", nextDate(28, 2, 2003));
  //  Case 2        D2max    M1       Y2
  ASSERT_EQ("2000-3-1", nextDate(29, 2, 2000));
  //  Case 3        D2max    M1       Y1
  ASSERT_EQ("2000-3-1", nextDate(29, 2, 2000));
  //  Case 4        D4max    M3max    Y3
  ASSERT_EQ("2004-1-1", nextDate(31, 12, 2003));
  //  Case 4        D3max    M2       Y3
  ASSERT_EQ("2003-12-1", nextDate(30, 11, 2003));
}

