#include "../date.hpp"
#include <string>

std::string printDate(Date aDate){
  return (std::to_string(aDate.year) + "-" + std::to_string(aDate.month) + "-" + std::to_string(aDate.day));;
}

std::string integrationNextDate(){
  Date aDate;
  aDate.Day = 2;
  aDate.Month = 2;
  aDate.Year = 2000;
  return printDate(aDate);
}

std::string stub3Driver(){
    return integrationNextDate();
}
