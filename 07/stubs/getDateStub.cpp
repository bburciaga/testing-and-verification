#include "../date.hpp"

Date getDate(Date aDate){
  return aDate;
}

Date integrationNextDate(){
  Date aDate;
  aDate.day = 2;
  aDate.month = 2;
  aDate.year = 2000;
  return getDate(aDate);
}

Date stub1Driver(){
  return integrationNextDate();
}
