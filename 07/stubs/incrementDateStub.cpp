#include "../date.hpp"

Date incrementDate(Date aDate){
  return aDate.day + 1;
}

Date integrationNextDate(){
  Date aDate;
  aDate.day = 2;
  aDate.month = 2;
  aDate.year = 2000;
  return incrementDate(aDate);
}

Date stub2Driver(){
  return integrationNextDate();
}
