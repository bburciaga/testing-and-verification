// Copyright Brian Burciaga 2022.                                                                                                          
/**
 * @file tests.cpp
 *
 * @brief File used to test commission function using slice testing
 *
 * @author Brian Burciaga T00566000
 * Contact: burciagab16@mytru.ca
 *
 */
#include <gtest/gtest.h>
#include <sstream>
#include "date.hpp"

TEST(IntegrationTesting, incrementDateStub) {
  std::istringstream sin("1\n16\n2005\n");
  auto cin_rdbuf = std::cin.rdbuf(sin.rdbuf());
  Date aDate;
  aDate.getDate();
  aDate.incrementDate();
  EXPECT_EQ(aDate.year, 2005);
  EXPECT_EQ(aDate.month, 1);
  EXPECT_EQ(aDate.day, 17);
  std::cin.rdbuf(cin_rdbuf);
}

TEST(IntegrationTesting, getDateStub) {
  std::istringstream sin("1\n16\n2005\n");
  auto cin_rdbuf = std::cin.rdbuf(sin.rdbuf());
  Date aDate;
  aDate.getDate();
  EXPECT_EQ(aDate.year, 2005);
  EXPECT_EQ(aDate.month, 1);
  EXPECT_EQ(aDate.day, 16);
  std::cin.rdbuf(cin_rdbuf);
}

TEST(IntegrationTesting, printDateStub) {
  Date aDate;
  std::istringstream sin("1\n16\n2005\n");
  auto cin_rdbuf = std::cin.rdbuf(sin.rdbuf());
  aDate.getDate();
  std::cin.rdbuf(cin_rdbuf);
  testing::internal::CaptureStdout();
  aDate.printDate();
  std::string output = testing::internal::GetCapturedStdout();
  std::string expected = "2005-1-16\n";
  EXPECT_EQ(output, expected);
}

TEST(IntegrationTesting, isLastDayOfMonthStub) {
  std::istringstream sin("1\n16\n2005\n");
  auto cin_rdbuf = std::cin.rdbuf(sin.rdbuf());
  Date aDate;
  aDate.getDate();
  int isTrue = aDate.isLastDateOfMonth(16, 1, 2005);
  EXPECT_EQ(isTrue, 1);
  std::cin.rdbuf(cin_rdbuf);
}

TEST(IntegrationTesting, isLeapYearStub) {
  std::istringstream sin("1\n16\n2005\n");
  auto cin_rdbuf = std::cin.rdbuf(sin.rdbuf());
  Date aDate;
  aDate.getDate();
  int isTrue = aDate.isLeap(2005);
  EXPECT_EQ(isTrue, 1);
  std::cin.rdbuf(cin_rdbuf);
}


