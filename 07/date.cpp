/**
 * @file nextDate.cpp
 *
 * @brief Contains function to get the next date with helper function
 *
 * @author Brian Burciaga T00566000
 * Contact: burciagab16@mytru.ca
 *
 */
#include <iostream>
#include <string>
#include "date.hpp"

Date::Date(){};
Date::Date(int d, int m, int y) {
  day = d;
  month = m;
  year = y;
};

int Date::isLeap(int y) {
  if (y % 4 == 0) {
    if (y % 100 == 0) {
      if (y % 400 == 0) {
        return 1;
      }
      return 0;
    }
    return 1;
  }
  return 0;
}
/**
 * isValidDate
 * @description check to see if the inputed date is valid in regards to the months
 * @param d day of the month
 * @param m month of the year
 * @param isLeapYear used to check February in case of leap year
 * @return true or false if the date is valid 
 */
int Date::isValidDate(int d, int m, int y) {
  if (m >= 1 && m <= 12 && d >= 1 && d <= 31 && y >= 1812 && y <= 2012) {
    switch (m) {
      case 2: 
        if (isLeap(y) && d < 30) return 1;
        if (d < 29) return 1;
        break;
      case 4:
      case 6:
      case 9:
      case 11:
        if (d <= 30) return 1;
        break;
      default:
        return 1; 
    }
  }
  return 0;
}

int Date::incrementNum (int n) {
  return n+1;
}

int Date::resetNum () {
  return 1;
}

int Date::isLastDayOfMonth(int d, int m, int isLeapYear) {
  switch (m) {
    case 2:
      if (isLeapYear && d == 29) return 1;
      if (d == 28) return 1;
    case 4:
    case 6:
    case 9:
    case 11:
      if (d == 30) return 1;
    default:
      if (d == 31) return 1;
  }
  return 0;
}

void Date::incrementDate() {
  if (isLastDayOfMonth(this->day, this->month, isLeap(this->year))) {
    this->day = resetNum();
    if (this->month == 12) {
      this->month = resetNum();
      this->year = this->incrementNum(this->year);
    } else this->month = this->incrementNum(this->month);
  } else this->day = incrementNum(this->day);

  if (this->month > 12) {
    this->month = this->resetNum();
    this->year = this->incrementNum(this->year);
  }
}

void Date::getDate() {
  int d;
  int m;
  int y;
  
  do {
    std::cout << "Enter a month: ";
    std::cin >> m;
    std::cout << "Enter a day: ";
    std::cin >> d;
    std::cout << "Enter a year: ";
    std::cin >> y;
  } while(!isValidDate(d, m, y));

  this->day = d;
  this->month = m;
  this->year = y;
}

void Date::printDate() {
  // Checking if it's a valid input
  std::cout << this->year << "-" 
    << this->month << "-" 
    << this->day << std::endl;
}

void Date::integrationNextDate() {
  Date aDate;
  std::cout << "Welcome to NextDate!" << std::endl;
  aDate.getDate();
  aDate.printDate();
  aDate.incrementDate();
  aDate.printDate();
}
