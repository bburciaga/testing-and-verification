/**
 * @file nextDate.hpp
 *
 * @brief header file to call the nextDate function in other directories
 *
 * @author Brian Burciaga T00566000
 * Contact: burciagab16@mytru.ca
 * 
 */
#ifndef DATE_HPP
#define DATE_HPP

class Date {
  public:
    int day;
    int month;
    int year;
    Date();
    Date(int d, int m, int y);
    int isLeap(int y);
    int isLastDayOfMonth(int d, int m, int isLeapYear);
    int isValidDate(int d, int m, int y);
    void incrementDate();
    void getDate();
    void printDate();
    void integrationNextDate();
  private:
    int resetNum();
    int incrementNum(int n);
};

#endif

