#include <string>

/**
 * Call function when wanting to print out Hello World! or
 * when you want to add Hello World! to a string variable.
 */
std::string helloworld (); 
