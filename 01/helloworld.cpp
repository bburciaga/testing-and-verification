#include "helloworld.h"
#include <iostream>
#include <gtest/gtest.h>

/**
 * void function to return string.
 * 
 * @return string "Hello World!".
 */
std::string helloworld () {
	return std::string("Hello World!");
}

TEST(HelloWorldTest, ReturnString) {
  // Testing helloworld () to see if it returns
  // "Hello World!"
	ASSERT_EQ("Hello World!", helloworld());
}

int main(int argc, char **argv) {
	testing::InitGoogleTest(&argc, argv);
  // Prints Hello World!
	std::cout << helloworld();
	return RUN_ALL_TESTS();
}
