vars=()
for var in "$@"
do
    if [[ "$var" != "=" ]]; then
      var1=${var%=*}
      var2=${var#*=}
      if [ ! -z "$var1" ] && [ ! -z "$var2" ]; then
        vars+=("$var")
      fi
    fi
done

printf "{\n"
let len=${#vars[@]}-1
for (( i=0; i<$len; i++ )); do
  var=${vars[$i]}
  var1=${var%=*}
  var2=${var#*=}
  printf "\t\"$var1\" : \"$var2\",\n"
done
var=${vars[$len]}
var1=${var%=*}
var2=${var#*=}
printf "\t\"$var1\" : \"$var2\"\n"

printf "}\n"

