# Docker Container

This container consists of the following:
    cmake
    vim
    google
    tests
    c++

The purpose of this container is to be used as an environment for testing
C++ code using the google test software.

As well I will be using this repository for understanding how building 
containers work as well.

