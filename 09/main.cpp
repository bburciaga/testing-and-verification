/**                                                                                                                                                                                                                                           
 * @file main.hpp
 *
 * @brief A simple program that loads a square root plugin and calculate the square root of a number
 *
 * @author Brian Burciaga T00566000
 * Contact: burciagab16@mytru.ca
 * 
 */

#include <iostream>
#include <dlfcn.h>
#include <cstdlib>
#include "SquareRoot.hpp"
#include "SquareRootFactory.hpp"

int main(int argc, char **argv) {
	std::string libstr = std::string("/usr/lib/SquareRoot/libcmath.so"); // "/usr/lib/SquareRoot/libnewton.so"
	double y;
	if (argc==1) {
		std::cerr << "Wrong number of arguments." << std::endl;
		return 1;
	} else if (argc==2) {
		y=atof(argv[1]);
	} else if (argc==3) {
		y=atof(argv[1]);
		libstr = std::string(argv[2]);
	} else {
		std::cerr << "Wrong number of arguments." << std::endl;
		return 1;
	}
    
    void *lib = dlopen(libstr.c_str(),RTLD_LAZY);
    if (!lib) {
        std::cerr << "Cannot load library" << std::endl;
        return 1;
    }
    dlerror(); //reset errors

    // load the symbols
    sr::create_t* create_squareroot = (sr::create_t*) dlsym(lib, "create");
    const char* dlsym_error = dlerror();
    if (dlsym_error) {
        std::cerr << "Cannot load symbol create: " << dlsym_error << '\n';
        return 1;
    }

    sr::destroy_t* destroy_squareroot = (sr::destroy_t*) dlsym(lib, "destroy");
    dlsym_error = dlerror();
    if (dlsym_error) {
        std::cerr << "Cannot load symbol destroy: " << dlsym_error << '\n';
        return 1;
    }

    // create an instance of the class
    sr::SquareRoot* f = create_squareroot();
    std::cout << f->message(y) << std::endl;

    destroy_squareroot(f);

    dlclose(lib);



    return 0;
}
