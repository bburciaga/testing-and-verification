/**                                                                                                                                                                                                                                           
 * @file newtonCubeRoot.cpp
 *
 * @brief used to define functions from class file
 *
 * @author Brian Burciaga T00566000
 * Contact: burciagab16@mytru.ca
 * 
 */

#include "newtonCubeRoot.hpp"
#include <cmath>

namespace cr {

NewtonCubeRoot::NewtonCubeRoot() : NewtonCubeRoot(0.00001) {}

NewtonCubeRoot::NewtonCubeRoot(double epsilon): epsilon_(epsilon) {
  std::cout << "NewtonSquareRoot Constructor" << std::endl;
}

NewtonCubeRoot::~NewstonCubeRoot() {
  std::cout << "NewtonSquareRoot Destructor" << std::endl;
}

double NewtonCubeRoot::calc(double x) {
  if (x < 0) return NAN;
  return calc(x, x/2);
}

double NewtonCubeRoot::calc(double x, double init) {
  if (fabs(init * init * init - x) <= epsilon_)
    return init;
  else {
    init = ((2.0 / 3.0 * init) + (x / (3.0 * init * init)));
    return calc(x, init);
  }
}

}
