/**                                                                                                                                                                                                                                           
 * @file newtonCubeRoot.hpp
 *
 * @brief header file that inherits the squareRoot class
 *
 * @author Brian Burciaga T00566000
 * Contact: burciagab16@mytru.ca
 * 
 */

#ifndef NEWTON_CUBE_ROOT_HPP
#define NEWTON_CUBE_ROOT_HPP

#include "SquareRoot.hpp"

namespace cr {

class NewtonCubeRoot : public SquareRoot {
public:
  NewtonCubeRoot();
  NewtonCubeRoot(double epsilon);
  ~NewtonCubeRoot();
  double calc(double x) override;
private:
  double calc(double x, double init);
  double epsilon_;
};

}

#endif
