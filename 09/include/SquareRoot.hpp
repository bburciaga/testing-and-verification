#ifndef SQUARE_ROOT_HPP
#define SQUARE_ROOT_HPP

#include <string>
#include <iostream>

namespace sr {

class SquareRoot {

public:
	SquareRoot();
    virtual double calc(double x)=0;
    virtual ~SquareRoot();
    std::string message(double x);
};

} //namespace sr
#endif //SQUARE_ROOT_HPP

