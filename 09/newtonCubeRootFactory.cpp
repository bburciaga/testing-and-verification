/**                                                                                                                                                                                                                                           
 * @file newtonCubeRootFactory.cpp
 *
 * @brief factory file for newtonCubeRoot class
 *
 * @author Brian Burciaga T00566000
 * Contact: burciagab16@mytru.ca
 * 
 */

#include "newtonCubeRoot.hpp"

namespace cr {

extern "C" SquareRoot* create() {
  return new NewtonCubeRoot;
}

extern "C" void destroy(SquareRoot* f) {
  delete f;
}

}
