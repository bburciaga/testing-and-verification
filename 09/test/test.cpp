/**                                                                                                                                                                                                                                           
 * @file test.cpp
 *
 * @brief Test file to test cubeRoot function
 *
 * @author Brian Burciaga T00566000
 * Contact: burciagab16@mytru.ca
 * 
 */

#include <gtest/gtest.h>

#include "newtonCubeRoot.hpp"

TEST(CubeRoot, Newton) {
  double eps = 0.00001;
  cr::NewtonCubeRoot f(eps);
  
  for (double d; d < 10; d++)
    EXPECT_NEAR(f.calc(d*d*d), d, eps);
}
