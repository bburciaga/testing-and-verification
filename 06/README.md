# Lab 6: Commission

The purpose of this project is to 
split the commission function into
slices and run tests based on the
slice functions.

As well we also learn to use the 
linting tool `cpplint` and `clang`
tool to have cleaner and less 
cluttered code.

### CPPLint

This is a command-line tool used to help fix
styling issues in C/C++ code following the
Google styling standards.

### CLang-format/CLang-tidy

`clang-format` Used to automatically reformat code based on styling standards.
`clang-tidy` Used to diagnose and fix programming errors

### Commission function

Function asks the user to input a value for the locks, stocks, and barrels.
After the user types -1 for locks the program calculates and returns the users
commission from the total sales.

#### Conditions

User has to sell at least 1 lock and 1 barrel, but not specifically one rifle.
The user cannot sell more than 70 locks, 80 stocks, and 90 barrels.
Commission varies based on how much you sell in costs.

### Testing Method

We use Slice testing to determine how the lock, total_locks, and total_lock_sales
variables are handled in the program.
