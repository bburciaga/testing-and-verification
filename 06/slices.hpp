/**
 * @file slices.hpp
 *
 * @brief Header file used so slice functions can be called by other files
 *
 * @author Brian Burciaga T00566000
 * Contact: burciagab16@mytru.ca
 *
 */
#include <iostream>
#include <string>

#ifndef SLICES_HPP
#define SLICES_HPP

int slice1();

int slice2();

int slice3or5();

int slice4();

std::string slice6();

int slice7();

#endif
