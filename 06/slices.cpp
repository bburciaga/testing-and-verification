/**
 * @file slices.cpp
 *
 * @brief functions to be used as slices in slice testing
 *
 * @author Brian Burciaga T00566000
 * Contact: burciagab16@mytru.ca
 *
 */
#include <iostream>
#include <string>

/**
 * slice test to check what happens if no input happens
 * @returns integer value 0
 */
int slice1() {
  int locks = 0;
  return locks;
}

/**
 * slice test to check what happens if the user inputs 
 * a value
 * @returns inputed integer value
 */ 
int slice2() {
  int locks;
  std::cin >> locks;

  return locks;
}

/**
 * slice test to check if a use inputs -1 or a number
 * and test how the loop would work
 * slice 3 and slice 5 are the same
 * @returns inputed integer value -1
 */ 
int slice3or5() {
  int locks = slice2();
  while(locks != -1) {
    std::cout << locks << std::endl;
    std::cin >> locks;
  }

  return locks;
}

/**
 * slice test to check if the inputed numbers can be added
 * to the total_locks value
 * @returns sum of inputed values not equal to -1
 */ 
int slice4() {
  int locks = slice2();
  int total_locks = slice1();

  while (locks != -1) {
    total_locks = total_locks + locks;
    std::cin >> locks;
  }
  
  return total_locks;
}

/**
 * slice to test if slice 4 can be outputed  with a string message
 * @returns string value with the total locks sold
 */ 
std::string slice6() {
  return "Locks sold: " + std::to_string(slice4());
}

/**
 * slice to test the total cost of the locks
 * @returns integer value of total cost of inputed locks
 */ 
int slice7() {
  return slice4() * 45.00;
}
