/**
 * @file commission.hpp
 *
 * @brief header file for the commission function in order for other files to call the function 
 *
 * @author Brian Burciaga T00566000
 * Contact: burciagab16@mytru.ca
 * 
 */
#include <iostream>
#include <string>

#ifndef COMMISSION_HPP
#define COMMISSION_HPP

std::string commission(int a, int b, int c);

#endif

