// Copyright Brian Burciaga 2022.
/**
 * @file tests.cpp
 *
 * @brief File used to test commission function using slice testing
 *
 * @author Brian Burciaga T00566000
 * Contact: burciagab16@mytru.ca
 *
 */
#include <gtest/gtest.h>
#include "./slices.hpp"

TEST(commissionTests, Slice1) {
  // Temporarily redirecting cin to simulate user inputs.
  std::istringstream sin("9");
  auto *cin_rdbuf = std::cin.rdbuf(sin.rdbuf());
  ASSERT_NE(9, slice1());
  // Resetting cin and cout
  std::cin.rdbuf(cin_rdbuf);
}

TEST(commissionTests, Slice2) {
  // Temporarily redirecting cin to simulate user inputs.
  std::istringstream sin("9");
  auto *cin_rdbuf = std::cin.rdbuf(sin.rdbuf());
  ASSERT_EQ(9, slice2());
  // Resetting cin and cout
  std::cin.rdbuf(cin_rdbuf);
}

TEST(commissionTests, Slice3) {
  // Temporarily redirecting cin to simulate user inputs.
  std::istringstream sin("-1");
  auto *cin_rdbuf = std::cin.rdbuf(sin.rdbuf());
  ASSERT_EQ(-1, slice3or5());
  // Resetting cin and cout
  std::cin.rdbuf(cin_rdbuf);
}

TEST(commissionTests, Slice4) {
  // Temporarily redirecting cin to simulate user inputs.
  std::istringstream sin("9 10 5 -1");
  auto *cin_rdbuf = std::cin.rdbuf(sin.rdbuf());
  ASSERT_EQ(24, slice4());
  // Resetting cin and cout
  std::cin.rdbuf(cin_rdbuf);
}

TEST(commissionTests, Slice5) {
  // Temporarily redirecting cin to simulate user inputs.
  std::istringstream sin("4 5 1 -1");
  auto *cin_rdbuf = std::cin.rdbuf(sin.rdbuf());
  ASSERT_EQ(-1, slice3or5());
  // Resetting cin and cout
  std::cin.rdbuf(cin_rdbuf);
}

TEST(commissionTests, Slice6) {
  // Temporarily redirecting cin to simulate user inputs.
  std::istringstream sin("4 5 1 -1");
  auto *cin_rdbuf = std::cin.rdbuf(sin.rdbuf());
  ASSERT_EQ("Locks sold: 10", slice6());
  // Resetting cin and cout
  std::cin.rdbuf(cin_rdbuf);
}

TEST(commissionTests, Slice7) {
  // Temporarily redirecting cin to simulate user inputs.
  std::istringstream sin("19 20 26 -1");
  auto *cin_rdbuf = std::cin.rdbuf(sin.rdbuf());
  ASSERT_EQ(2925, slice7());
  // Resetting cin and cout
  std::cin.rdbuf(cin_rdbuf);
}
