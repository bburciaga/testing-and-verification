
 ##
 # @file CMakeLists.txt
 #
 # @brief function used to generate google tests and execution files for tests
 #
 # @author Brian Burciaga T00566000
 # Contact: burciagab16@mytru.ca
 #
 #

# minimum version to use for cmake
cmake_minimum_required(VERSION 3.10)
# project name
project(lab5)

# CPack Package Definition
set(CPACK_PACKAGE_NAME "commission")
set(CPACK_PACKAGE_NAME "slices")
set(CPACK_PACKAGE_VERSION_MAJOR 1)
set(CPACK_PACKAGE_VERSION_MINOR 0)
set(CPACK_PACKAGE_VERSION_PATCH 0)
set(CPACK_PACKAGE_VENDOR "seng4110")
set(CPACK_PACKA GE_CONTANCT "gefink@tru.ca")
set(CPACK_GENERATOR "DEB")
set(CPACK_DEBIAN_PACKAGE_MAINTAINER "Brian Burciaga")
set(CPACK_DEBIAN_FILE_NAME "commission.deb")
set(CPACK_DEBIAN_FILE_NAME "slices.deb")
set(CPACK_PACKAGING_INSTALL_PREFIX "/usr/local")

# include package for project
find_package(GTest REQUIRED)
include_directories(${GTEST_INCLUDE_DIRS})

add_library(commission_library commission.cpp commission.hpp)
add_library(slices_library slices.cpp slices.hpp)
install(TARGETS slices_library DESTINATION ${CMAKE_INSTALL_BINDIR})
install(TARGETS commission_library DESTINATION ${CMAKE_INSTALL_BINDIR})

# generating executable for the cpp program
add_executable(runTests main.cpp tests.cpp slices.cpp)
install(TARGETS runTests DESTINATION ${CMAKE_INSTALL_BINDIR})
target_link_libraries(runTests slices_library)
target_link_libraries(runTests ${GTEST_LIBRARIES} pthread)

include(CPack)
