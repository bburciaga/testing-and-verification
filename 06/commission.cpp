/**
 * @file commission.cpp
 *
 * @brief Function to get the commission of the user for his sales.
 *
 * @author Brian Burciaga T00566000
 * Contact: burciagab16@mytru.ca
 *
 */
#include <iostream>
#include <string>

#define MAX_LOCKS 70
#define MAX_STOCKS 80
#define MAX_BARRELS 90

#define LOCK_PRICE 45.00
#define STOCK_PRICE 35.00
#define BARREL_PRICE 25.00

/**
 * Function used to ask the user for number of sales for
 * locks, stocks, and barrels.
 */
void commission () {
  int locks;
  int barrels;
  int stocks;

  int total_barrels = 0;
  int total_locks = 0;
  int total_stocks = 0;

  std::cin >> locks;

  while (locks != -1) {
    std::cin >> barrels;
    total_barrels = total_barrels + barrels;
    std::cin >> stocks;
    total_stocks = total_stocks + stocks;

    total_locks = total_locks + locks;
    std::cin >> locks;
  }

  std::cout << "Locks Sold: " << total_locks << std::endl;
  std::cout << "Stocks Sold: " << total_stocks << std::endl;
  std::cout << "Barrels Sold: " << total_barrels << std::endl;

  float lock_sales = total_locks * LOCK_PRICE;
  float stock_sales = total_stocks * STOCK_PRICE;
  float barrel_sales = total_barrels * BARREL_PRICE;

  float sales = lock_sales + stock_sales + barrel_sales;
  float commission;

  if (sales > 1800.00) {
    commission = 0.10 * 1000.00;
    commission = commission + 0.15 * 800.00;
    commission = commission + 0.20 * (sales - 1800.00);
  } else if (sales > 1000.00) {
    commission = 0.10 * 1000.00;
    commission = commission + 0.15 * (sales - 1000.00);
  }
  commission = 0.10 * sales;

  std::string commission_str = std::to_string(commission);

  std::cout << "Commission: $" << commission_str;
}
